package cl.itf.mana.client.model;

/**
 * @author lebedev-m
 */
public class Connection {

    //---------------------------------------------------
    private boolean uselogin;

    public boolean isUselogin() {
        return uselogin;
    }

    public void setUselogin(boolean uselogin) {
        this.uselogin = uselogin;
    }

    //---------------------------------------------------
    private boolean serverMode;

    public boolean isServerMode() {
        return serverMode;
    }

    public void setServerMode(boolean serverMode) {
        this.serverMode = serverMode;
    }

    //---------------------------------------------------
    private boolean filemode = true;

    public boolean isFilemode() {
        return filemode;
    }

    public void setFilemode(boolean filemode) {
        this.filemode = filemode;
    }

    //---------------------------------------------------
    private String filepath;

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    //---------------------------------------------------
    private String server;

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    //---------------------------------------------------
    private String base;

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    //---------------------------------------------------
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    //---------------------------------------------------
    private String passwd;

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    //---------------------------------------------------
    private String remServer;

    public String getRemServer() {
        return remServer;
    }

    public void setRemServer(String remServer) {
        this.remServer = remServer;
    }

    //---------------------------------------------------
    private String remPort;

    public String getRemPort() {
        return remPort;
    }

    public void setRemPort(String remPort) {
        this.remPort = remPort;
    }

    //---------------------------------------------------
    private String label;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
    //---------------------------------------------------
}
