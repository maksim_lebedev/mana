package cl.itf.mana.client.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lebedev-m
 */
public class Base implements Serializable {

    private static final long serialVersionUID = -1987584715899181397L;

    //-----------------------------------------------------
    private boolean mark;

    public boolean isMark() {
        return mark;
    }

    public void setMark(boolean mark) {
        this.mark = mark;
    }
    
    //-----------------------------------------------------
    private String label;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    //-----------------------------------------------------
    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    //-----------------------------------------------------
    private String company;

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    //-----------------------------------------------------
    private int reports;

    public int getReports() {
        return reports;
    }

    public void setReports(int reports) {
        this.reports = reports;
    }

    //-----------------------------------------------------
    private String saveTo;

    public String getSaveTo() {
        return saveTo;
    }

    public void setSaveTo(String saveTo) {
        this.saveTo = saveTo;
    }
    //-----------------------------------------------------

    private Connection connection;

    public Connection getConnectData() {
        return connection;
    }

    public void setConnectData(Connection connection) {
        this.connection = connection;
    }
    //-----------------------------------------------------

    private final List<Report> replist = new ArrayList<>();

    public List<Report> getReplist() {
        return replist;
    }

    public void setReplist(List<Report> replist) {
        this.replist.clear();
        this.replist.addAll(replist);
        setReports(this.replist.size());
    }
    
    //-----------------------------------------------------
}
