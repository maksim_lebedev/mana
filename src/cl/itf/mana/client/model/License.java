package cl.itf.mana.client.model;

/**
 * @author sniffl
 */
public class License {
//    {"response":"not registered",
//     "status":"Приложение не зарегистрировано. Неверный ключ.",
//     "owner": "ООО "Радико""
//     "timestamp":1426537641}
    
    private final String response;
    private final String status;
    private final String owner;
    private final long timestamp;

    public License(String response, String status, String owner, long timestamp) {
        this.response = response;
        this.status = status;
        this.owner = owner;
        this.timestamp = timestamp;
    }

    public String getResponse() {
        return response;
    }

    public boolean isRegistered() {
        return "registered".equals(response);
    }

    public String getStatus() {
        return status;
    }

    public String getOwner() {
        return owner;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
