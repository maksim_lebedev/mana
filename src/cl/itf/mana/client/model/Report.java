package cl.itf.mana.client.model;

import java.io.Serializable;

/**
 * @author lebedev-m
 */
public class Report implements Serializable {

    private static final long serialVersionUID = -8513836006441252677L;

    //-----------------------------------------------------
    private boolean mark;

    public static final String PROP_MARK = "mark";

    public boolean isMark() {
        return mark;
    }

    public void setMark(boolean mark) {
        this.mark = mark;
    }
    
    //-------------------------------------------
    private String name;

    public static final String PROP_NAME = "name";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //-------------------------------------------
    private String period;

    public static final String PROP_PERIOD = "period";

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    //-------------------------------------------
    private String value;

    public static final String PROP_VALUE = "value";

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
    //-------------------------------------------
    private int type;

    public static final String PROP_TYPE = "type";

    public String getType() {
        return String.valueOf(type);
    }

    public void setType(int type) {
        this.type = type;
    }
}
