package cl.itf.mana.client.socket;

import java.net.*;
import org.slf4j.LoggerFactory;

/**
 * Encapsulates and manages a pair of inbound and outbound
 * transfer agents.
 */
public class TransferManager
{
    protected org.slf4j.Logger logger = LoggerFactory.getLogger( TransferManager.class );
    
    private final TransferAgent agentIn;
    private final TransferAgent agentOut;

    /** 
     * Constructs a TransferManager instance.
     * @param sockIn inbound socket
     * @param sockOut outbound socket
     * @param cfg configuration object
     * @throws IllegalAccessException illegal access exception
     * @throws InstantiationException instantiation exception
     */
    public TransferManager( Socket sockIn, Socket sockOut )
        throws IllegalAccessException, InstantiationException
    {
        //transfer agent to move data back to the client
        agentOut =
            new TransferAgent(
                sockOut,
                sockIn,
                null,
                false);

        //transfer agent to move data from the client to the server
        agentIn =
            new TransferAgent(
                sockIn,
                sockOut,
                null,
                true);
    }

    public void agentStart()
    {
        //start both agent threads
        agentOut.start();
        agentIn.start();

        //let the agent threads spin up before we return
        try
        {
            Thread.sleep(300);
        }
        catch (Exception e)
        { 
            //do nothing
        }
    }
    
    /** 
     * Initiates shutdown. 
     */
    public void shutdown()
    {
        //shutdown both agents within this fransfer manager
        agentIn.shutdown();
        agentOut.shutdown();
    }

    /**
     * Returns the state of the socket.
     * 
     * @return socket status
     */
    public boolean isClosed()
    {
        boolean isClosed = false;
        if (agentIn.isClosed() || agentOut.isClosed())
        {
            isClosed = true;
        }
        return isClosed;
    }
}
