package cl.itf.mana.client.socket;

import java.io.*;

/**
 * Sample/utility class that implements IDataHandler.
 * Converts the byte stream to String and writes 
 * it to a file.
 */
public class DataToFile implements IDataHandler
{
    /**
     * Constructs DataToFile.
     */
    public DataToFile()
    {
        //do nothing
    }

    /** 
     * Pushes incoming byte array to custom handler class.
     * The incoming bytes are then available to handler
     * logic for inspection and/or modification.
     * @return the original (or modified) array of bytes
     * @param bytes the inbound byte array
     * @throws IllegalStateException custom handler in an unready state
     */
    public byte[] pushBytes(byte[] bytes) throws IllegalStateException
    {
        //convert to String write to the file
        FileManager.getInstance().write(new String(bytes));
        //return the byte array
        return bytes;
    }
}

/**
 * Class to encapsulate file IO logic and
 * synchronize access to the file. 
 *
 */
class FileManager
{
    private static final FileManager INSTANCE = new FileManager();
    private static final String FILE = "activity.txt";
    private static DataOutputStream dos = null;
    private static PrintStream ps = null;
    private static boolean enabled = true;

    /**
     * Constructs a file manager.
     */
    private FileManager()
    {
        //singleton - no external instantiations allowed

        //prepare the print stream so we can write to the file
        try
        {
            dos = new DataOutputStream(new FileOutputStream(FILE, true));
            ps = new PrintStream(dos);
        }
        catch (FileNotFoundException fnfe)
        {
            enabled = false;
        }
        catch (Exception e)
        {
            enabled = false;
        }
    }

    /**
     * Gets the single instance of the file manager (singleton).
     * 
     * @return an instance of the FileManager
     */
    static FileManager getInstance()
    {
        if (enabled)
        {
            //hand out the singleton
            return INSTANCE;
        }
        else
        {
            throw new IllegalStateException("File not found.");
        }
    }

    /**
     * Writes to the file.
     * Synchronized for thread safety as multiple
     * threads may be writing to the file.
     *  
     * @param str the string being written
     */
    synchronized void write(String str)
    {
        if (ps != null)
        {
            //write string to the file
            ps.println(str);
        }
    }
}
