package cl.itf.mana.client.socket;

import java.io.*;
import java.net.*;
import org.slf4j.LoggerFactory;

/**
 * Manages the flow of data in a single direction. Connects the inbound socket
 * with the outbound socket.
 */
public class TransferAgent extends Thread
{
    protected org.slf4j.Logger logger = LoggerFactory.getLogger( TransferAgent.class );

    private Socket sockIn = null;
    private Socket sockOut = null;
    private IDataHandler listener = null;
    private boolean inbound = false;

    /**
     * Constructs a transfer agent.
     *
     * @param sockIn inbound socket
     * @param sockOut outbound socket
     * @param listener custom listener class
     * @param inbound true if agent manages data entering the proxy server
     * @throws IllegalArgumentException illegal argument
     * @throws NullPointerException null pointer
     */
    public TransferAgent(
            Socket sockIn,
            Socket sockOut,
            IDataHandler listener,
            boolean inbound )
            throws IllegalArgumentException, NullPointerException
    {
        //full parameter validation
        if ( sockIn == null )
        {
            throw new NullPointerException( "null input socket" );
        }

        if ( sockOut == null )
        {
            throw new NullPointerException( "null output socket" );
        }

        if ( listener == null )
        {
            throw new NullPointerException( "null IDataListener" );
        }

        if ( sockIn.isClosed() || !sockIn.isBound() || !sockIn.isConnected() )
        {
            throw new IllegalArgumentException( "broken input socket" );
        }

        if ( sockOut.isClosed() || !sockOut.isBound() || !sockOut.isConnected() )
        {
            throw new IllegalArgumentException( "broken output socket" );
        }

        //store params
        this.sockIn = sockIn;
        this.sockOut = sockOut;
        this.listener = listener;
        this.inbound = inbound;
    }

    /**
     * Runs the thread.
     */
    @Override
    public void run()
    {
        try
        {
            InputStream isIn = sockIn.getInputStream();
            OutputStream osOut = sockOut.getOutputStream();
            DataInputStream dis = new DataInputStream( isIn );
            DataOutputStream dos = new DataOutputStream( osOut );

            int byteCount;
            byte[] tmpba = null;

            //infinite loop
            while ( true )
            {
                byte[] ba = new byte[ sockIn.getReceiveBufferSize() ];
                //block and wait for data
                byteCount = dis.read( ba );

                if ( byteCount > 0 )
                {
                    //get rid of the extra space in the array
                    ba = Utility.compressByteArray( ba, byteCount );

                    try
                    {
                        //log the bytes
                        if ( inbound )
                        {
                            //use the inbound logger
                            logger.debug( new String( ba ) );
                        }
                        else
                        {
                            //use the outbound logger
                            logger.debug( new String( ba ) );
                        }

                        //submit the byte array to the designated handler
                        //(IDataListener) for custom processing
                        tmpba = listener.pushBytes( ba );
                    }
                    catch ( Exception e )
                    {
                        //we have to guard against ANY exception
                        //being thrown from the plugin code

                        //log the exception to the system log
                        logger.debug(
                                "Exception in plugin code caught: " + e );

                        //bail out of the loop and force socket closure
                        break;
                    }

                    //forward the returned byte array to the destination
                    dos.write( tmpba, 0, tmpba.length );
                }
                else if ( byteCount < 0 )
                {
                    //unable to read -- get out of the infinite loop
                    break;
                }
                else if ( byteCount == 0 )
                {
                    //nothing to do -- just read again
                }
            }
        }
        catch ( IOException e )
        {
            //nothing to do here
            //we've detected that the socket has been closed
            //or has a problem
        }
        finally
        {
            //ensure that both sockets are closed
            try
            {
                sockIn.close();
            }
            catch ( IOException ioe )
            {
                //do nothing - we are just making sure it is closed
            }

            try
            {
                sockOut.close();
            }
            catch ( IOException ioe )
            {
                //do nothing - we are just making sure it is closed
            }
        }

    }

    /**
     * Shuts down the transfer agent.
     */
    void shutdown()
    {
        //close the sockets
        try
        {
            sockIn.close();
        }
        catch ( IOException ioe )
        {
            //do nothing - we are just making sure it is closed
        }

        try
        {
            sockOut.close();
        }
        catch ( IOException ioe )
        {
            //do nothing - we are just making sure it is closed
        }
    }

    /**
     * Returns the state of the socket.
     *
     * @return socket status
     */
    boolean isClosed()
    {
        boolean isClosed = false;
        if ( sockIn.isClosed() || sockOut.isClosed() )
        {
            isClosed = true;
        }
        return isClosed;
    }
}
