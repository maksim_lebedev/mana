package cl.itf.mana.client.socket;

/**
 * Utility routines.
 */
class Utility
{
    /**
     * Constructor.
     */
    private Utility()
    {
        //do nothing
    }

    /**
     * Shrinks the physical size of the byte array (not content).
     * 
     * @param ba the byte array
     * @param len the length of the byte array
     * @return the new (compressed) byte array
     */
    static synchronized byte[] compressByteArray(byte[] ba, int len)
    {
        byte[] ba2 = new byte[len];
        System.arraycopy( ba, 0, ba2, 0, len );
        //return the downsized byte array
        return ba2;
    }
}
