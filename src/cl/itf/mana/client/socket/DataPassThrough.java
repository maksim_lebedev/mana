package cl.itf.mana.client.socket;

/**
 * Sample/utility class that implements IDataHandler.
 * Passes data straight through without modification.
 */
public class DataPassThrough implements IDataHandler
{
    /**
     *  Constructs DataPassThrough.
     *
     */
    public DataPassThrough()
    {
        //do nothing on construction
    }

    /**
     * Pushes incoming byte array to custom handler class.
     * The incoming bytes are then available to handler
     * logic for inspection and/or modification.
     * 
     * @param bytes the inbound byte array
     * @return the original (or modified) array of bytes
     */
    public byte[] pushBytes(byte[] bytes)
    {
        //no action taken
        //simply return the byte array
        return bytes;
    }
}
