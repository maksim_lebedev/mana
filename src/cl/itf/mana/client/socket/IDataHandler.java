package cl.itf.mana.client.socket;

/**
 * Interface that must be implemented by every custom listener class.
 * This method allows for customization to take place.  For example, the 
 * byte array may be examined for information that triggers custom
 * processing.  A byte array must be returned.  The returned byte array
 * may be exactly the same as was received, or may be modified by custom
 * processing logic.
 */
public interface IDataHandler
{
    /**
     * Pushes incoming byte array to custom listener class.
     * The incoming bytes are then available to listener
     * logic for inspection and/or modification.
     * 
     * @param bytes the inbound byte array
     * @return the original (or modified) array of bytes
     */
    byte[] pushBytes(byte[] bytes);
}
