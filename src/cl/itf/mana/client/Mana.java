package cl.itf.mana.client;

import cl.itf.mana.client.ui.EscapeNotifController;
import cl.itf.mana.client.ui.LicenseKeyController;
import cl.itf.mana.client.ui.ManaTabPaneController;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

/**
 * @author max
 */
public class Mana extends Application {

    private static Properties properties;
    private Stage stage;
    private ResourceBundle resources;

    /**
     * start. Build UI and show here.
     *
     * @param stage
     */
    @Override
    public void start(Stage stage) {        
        final Locale locale = Locale.getDefault();
        resources = ResourceBundle.getBundle("cl/itf/mana/client/localization/itf", locale);
        
        this.stage = stage;
        
        String license = existFileLicense();
        if (license == null) {
            initQueryLicenseLayout();
        } else {
            initTabPane(license);
        }

        this.stage.show();
    }

    private String existFileLicense() {
        File licence = new File("key.license");
        String license = null;
        if (licence.exists()) {
            FileInputStream inFile = null;
            try {
                inFile = new FileInputStream(licence);
                byte[] str = new byte[inFile.available()];
                inFile.read(str);
                license = new String(str);
            } catch (Exception ex) {
                Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    if (inFile != null) {
                        inFile.close();
                    }
                } catch (IOException ex) {
                    Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return license;
    }
    
    /** */
    public void initQueryLicenseLayout() {
        try {            
            final LicenseKeyController licController = 
                    (LicenseKeyController) replaceSceneContent("ui/LicenseKey.fxml");
            licController.setMainApp(this);
            this.stage.setTitle(resources.getString("not_registered"));
        } catch (Exception e) {
            Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    /** */
    public void initEscapeNotifLayout() {
        try {            
            final EscapeNotifController licController = 
                    (EscapeNotifController) replaceSceneContent("ui/EscapeNotif.fxml");
            licController.setMainApp(this);
            this.stage.setTitle(resources.getString("not_registered"));
        } catch (Exception e) {
            Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    /**
     * @param licence 
     */
    public void initTabPane(String licence) {
        try {
            final ManaTabPaneController controller = 
                    (ManaTabPaneController) replaceSceneContent("ui/ManaTabPane.fxml");
            controller.setMainApp(this);
            controller.loadScreens(licence);
            this.stage.setTitle("MANA");
            this.stage.setMinWidth(980d);
            this.stage.setMinHeight(700d);
            this.stage.setWidth(980d);
            this.stage.setHeight(700d);
            this.stage.setMaxWidth(980d);
            this.stage.setMaxHeight(700d);
            this.stage.setOnCloseRequest(controller);
        } catch (Exception e) {
            Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    private Node replaceSceneContent(String fxml) throws Exception {
        final FXMLLoader loader = new FXMLLoader();
        final InputStream in = Mana.class.getResourceAsStream(fxml);
        loader.setBuilderFactory(new JavaFXBuilderFactory());
        loader.setLocation(Mana.class.getResource(fxml));
        Region page;
        try {
            page = (Region) loader.load(in);
        } finally {
            in.close();
        }
        
        // Store the stage width and height in case the user has resized the window
        double stageWidth = stage.getWidth();
        if (!Double.isNaN(stageWidth)) {
            stageWidth -= (stage.getWidth() - stage.getScene().getWidth());
        }
        
        double stageHeight = stage.getHeight();
        if (!Double.isNaN(stageHeight)) {
            stageHeight -= (stage.getHeight() - stage.getScene().getHeight());
        }
        
        final Scene scene = new Scene(page);
        if (!Double.isNaN(stageWidth)) {
            page.setPrefWidth(stageWidth);
        }
        if (!Double.isNaN(stageHeight)) {
            page.setPrefHeight(stageHeight);
        }
        
        stage.setScene(scene);
        stage.sizeToScene();
        return (Node) loader.getController();
    }
    
    private void loadProperty() {
        final String name = "mana.properties";
        String filepath = System.getProperty("xl.file_properties");
        
        if (filepath == null) {
            filepath = System.getProperty("user.dir") + File.separatorChar + name;
        }
        final File file = new File(filepath);
        if (!file.exists()) {
            file.mkdirs();
        }
        
        try (BufferedReader fileInput = new BufferedReader(new FileReader(file))) {
            Properties p = new Properties();
            p.load(fileInput);

            if (!p.isEmpty()) {
                properties = p;
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Stage getStage() {
        return stage;
    }
    
    /**
     * Application Entry Point. Program starts here.
     *
     * @param args
     */
    public static void main(String[] args) {
        Application.launch(Mana.class, (java.lang.String[])null);
    }
}
