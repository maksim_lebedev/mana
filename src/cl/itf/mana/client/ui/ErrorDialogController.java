package cl.itf.mana.client.ui;

import cl.itf.mana.client.Mana;
import javafx.scene.layout.GridPane;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * @author max
 */
public class ErrorDialogController extends GridPane {
    
    private DialogService service;
    
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ImageView imgAlert;
    
    @FXML
    private Button cancelButton;

    @FXML
    private Label detailsLabel;

    @FXML
    private Label messageLabel;

    @FXML
    void onCancel(ActionEvent event) {
        service.hide();
    }
    
    @FXML
    void initialize() {
        final Locale locale = Locale.getDefault();
        resources = ResourceBundle.getBundle("cl/itf/mana/client/localization/itf", locale);
        
        String imgPath = "img/remove.png";
        Image image = new Image(Mana.class.getResourceAsStream(imgPath));
        imgAlert.setImage(image);
        
        this.cancelButton.setText(resources.getString("btn.close"));
    }
    
    public void setDialogService(DialogService service) {
        this.service = service;
    }
    
    public void setErrorMessage(String error) {
        this.messageLabel.setText(error);
    }
    
    public void setDetail(String detail) {
        this.detailsLabel.setText(detail);
    }
}
