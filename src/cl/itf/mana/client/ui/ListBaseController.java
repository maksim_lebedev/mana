package cl.itf.mana.client.ui;

import cl.itf.mana.client.model.Base;
import cl.itf.mana.client.model.Report;
import cl.itf.mana.client.util.TaskBasedSplash;
import cl.itf.mana.client.util.TaskBasedSplash.InitCompletionHandler;
import java.net.URL;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

/**
 * @author max
 */
public class ListBaseController implements IScreens<BaseScreensController>, InitCompletionHandler {

    private BaseScreensController screenParent;
    
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ListView<Base> listBases;

    @FXML
    void initialize() {
        updateLang();
        final ObservableList<Base> basesData = FXCollections.observableArrayList();
        listBases.setItems(basesData);
        listBases.setCellFactory(new Callback<ListView<Base>, ListCell<Base>>() {
            @Override
            public ListCell<Base> call(ListView<Base> list) {
                BaseRowCell baseRowCell = new BaseRowCell();
                baseRowCell.setScreenParent(screenParent);
                return baseRowCell;
            }
        });
    }
    
    @Override
    public void setScreenParent(BaseScreensController screenParent) {
        this.screenParent = screenParent;
    }
    
    @Override
    public void updateLang() {
        final Locale locale = Locale.getDefault();
        resources = ResourceBundle.getBundle("cl/itf/mana/client/localization/itf", locale);
    }
    
    public void repGen(boolean selected) {
        final TaskBasedSplash task = new TaskBasedSplash();
        task.startRepGen(getListBases(), selected, this);
    }
    
    public void addItem(Base item) {
        if (item != null) {
            final ObservableList<Base> items = getListBases();
            items.add(item);
            screenParent.updateChanges(items);
        }
    }

    public void updateItem(Base item) {
        if (item != null) {
            final ObservableList<Base> items = getListBases();
            final int index = items.indexOf(item);
            items.set(index, item);
            screenParent.updateChanges(items);
        }
    }
    
    public void updatePeriod(boolean selected, String date1, String date2) {
        final ObservableList<Base> items = getListBases();
        if (selected) {
            for (Base itf : items) {
                if (itf.isMark()) {
                    final List<Report> replist = itf.getReplist();
                    for (Report repSel : replist) {
                        repSel.setPeriod(date1 + " / " + date2);
                    }
                }
            }
        } else {
            for (Base itf : items) {
                final List<Report> replist = itf.getReplist();
                for (Report repSel : replist) {
                    repSel.setPeriod(date1 + " / " + date2);
                }
            }
        }
        
        screenParent.updateChanges(items);
    }
    
    public void delBase(boolean selected) {
        final ObservableList<Base> items = getListBases();
        if (selected) {
            final int[] inds = new int[items.size()];
            for (Base itf : items) {
                if (itf.isMark()) {
                    int indexOf = items.indexOf(itf);
                    inds[indexOf] = 1;
                }
            }
            for (int i = inds.length; i > 0; i--) {
                int del = inds[i - 1];
                if (del == 1) {
                    items.remove(i - 1);
                }
            }
        } else {
            items.clear();
        }
        
        screenParent.updateChanges(items);
    }
        
    public boolean isEmpty() {
        final ObservableList<Base> items = getListBases();
        return items == null ? true : items.isEmpty();
    }
    
    public ObservableList<Base> getListBases() {
        return listBases.getItems();
    }
    
    @Override
    public void complete(Object value) {
        
    }

    @Override
    public void failed(String error) {
        screenParent.showErrorDialog(resources.getString("error"), error);
    }

    @Override
    public void cancelled() {
        
    }
}
