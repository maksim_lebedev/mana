package cl.itf.mana.client.ui;

import cl.itf.mana.client.Mana;
import cl.itf.mana.client.model.Base;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextField;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;

/**
 * @author max
 */
public class BaseRowCell extends ListCell<Base> {
    
    private BaseScreensController myController;
    private final ResourceBundle resources;
    private final GridPane _mComponent;
    private final CheckBox chCheck;
    private final Label lblName;
    private final Button btnInfo;
    private final TextField txtKolRep;
    private final Button btnRep;

    public BaseRowCell() {
        final Locale locale = Locale.getDefault();
        resources = ResourceBundle.getBundle("cl/itf/mana/client/localization/itf", locale);
        
        this._mComponent = new GridPane();
        this._mComponent.setAlignment(Pos.CENTER);
        this._mComponent.setPadding(new Insets(0, 10, 0, 10));
        this._mComponent.getStyleClass().add("background");
        
        RowConstraints rowConstraints = new RowConstraints();
        rowConstraints.setVgrow(Priority.SOMETIMES);
        rowConstraints.setMinHeight(10.0);
        rowConstraints.setPrefHeight(80.0);
        
        ColumnConstraints c1 = new ColumnConstraints();
        c1.setHgrow(Priority.NEVER);
        c1.setMinWidth(50.0);
        c1.setPrefWidth(50.0);
        c1.setMaxWidth(50.0);
        
        ColumnConstraints c2 = new ColumnConstraints();
        c2.setHgrow(Priority.ALWAYS);
        c2.setMinWidth(10.0);
        c2.setPrefWidth(100.0);
        
        ColumnConstraints c3 = new ColumnConstraints();
        c3.setHgrow(Priority.ALWAYS);
        c3.setMinWidth(10.0);
        c3.setPrefWidth(100.0);
        
        ColumnConstraints c4 = new ColumnConstraints();
        c4.setHgrow(Priority.ALWAYS);
        c4.setMinWidth(10.0);
        c4.setPrefWidth(100.0);
        
        ColumnConstraints c5 = new ColumnConstraints();
        c5.setHgrow(Priority.NEVER);
        
        ColumnConstraints c6 = new ColumnConstraints();
        c6.setHgrow(Priority.NEVER);
        
        ColumnConstraints c7 = new ColumnConstraints();
        c7.setHgrow(Priority.NEVER);
        
        this._mComponent.getRowConstraints().add(rowConstraints);
        this._mComponent.getColumnConstraints().addAll(c1, c2, c3, c4, c5, c6, c7);
        
        this.chCheck = new CheckBox();
        this.chCheck.setMnemonicParsing(false);
        this.chCheck.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Base item = getItem();
                CheckBox source = (CheckBox) event.getSource();
                item.setMark(source.isSelected());
            }
        });
        GridPane.setMargin(this.chCheck, new Insets(0, 10, 0, 10));
        this._mComponent.add(this.chCheck, 0, 0, 1, 1);
        
        this.lblName = new Label();
        this.lblName.setWrapText(true);
        this.lblName.getStyleClass().add("label-base");
        this._mComponent.add(this.lblName, 1, 0, 3, 1);

        this.txtKolRep = new TextField();
        this.txtKolRep.getStyleClass().add("textKol");
        this.txtKolRep.setAlignment(Pos.CENTER);
        this.txtKolRep.setEditable(false);
        this.txtKolRep.setMaxHeight(50.0);
        this.txtKolRep.setMaxWidth(50.0);
        this.txtKolRep.setMinHeight(50.0);
        this.txtKolRep.setMinWidth(50.0);
        this.txtKolRep.setPrefHeight(50.0);
        this.txtKolRep.setPrefWidth(50.0);
        GridPane.setMargin(this.txtKolRep, new Insets(0, 10, 0, 10));
        GridPane.setHalignment(this.txtKolRep, HPos.CENTER);
        this._mComponent.add(this.txtKolRep, 4, 0, 1, 1);
        
        this.btnInfo = new Button();
        this.btnInfo.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        this.btnInfo.setMnemonicParsing(false);
        this.btnInfo.setMaxHeight(50.0);
        this.btnInfo.setMaxWidth(50.0);
        this.btnInfo.setMinHeight(50.0);
        this.btnInfo.setMinWidth(50.0);
        this.btnInfo.setPrefHeight(50.0);
        this.btnInfo.setPrefWidth(50.0);
        String imgPath = "img/edit.png";
        Image image = new Image(Mana.class.getResourceAsStream(imgPath));
        final ImageView imageView = new ImageView(image);
        this.btnInfo.setGraphic(imageView);
        this.btnInfo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Base item = getItem();
                AddBaseController addBaseContr = 
                        (AddBaseController) myController.loadScreen(BaseScreensController.screenAddBaseID, 
                                                                    BaseScreensController.screenAddBaseFile);
                addBaseContr.edit(item);
                myController.setScreen(BaseScreensController.screenAddBaseID);
            }
        });
        GridPane.setMargin(this.btnInfo, new Insets(0, 10, 0, 10));
        GridPane.setHalignment(this.btnInfo, HPos.CENTER);
        this._mComponent.add(this.btnInfo, 5, 0, 1, 1);
        
        this.btnRep = new Button();
        this.btnRep.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        this.btnRep.setMnemonicParsing(false);
        this.btnRep.setMaxHeight(50.0);
        this.btnRep.setMaxWidth(50.0);
        this.btnRep.setMinHeight(50.0);
        this.btnRep.setMinWidth(50.0);
        this.btnRep.setPrefHeight(50.0);
        this.btnRep.setPrefWidth(50.0);
        imgPath = "img/details.png";
        image = new Image(Mana.class.getResourceAsStream(imgPath));
        final ImageView imageView1 = new ImageView(image);
        this.btnRep.setGraphic(imageView1);
        this.btnRep.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    final RepToolBarController repController
                                               = (RepToolBarController) myController.getTabController().replaceToolBar(BaseScreensController.screenRepTbID,
                                                                                                                       BaseScreensController.screenRepTbFile);
                    String name;
                    final Base itf = getItem();
                    repController.initAttr(itf);
                    
                    if (itf.getReplist().isEmpty()) {
                        name = BaseScreensController.screenEmptyRepID;
                        final EmptyRepController controller
                                                 = (EmptyRepController) myController.loadScreen(name, BaseScreensController.screenEmptyRepFile);
                        controller.setStRepList(repController.getStRepList());
                        controller.initAttr(itf);
                        myController.setScreen(name);
                    } else {
                        name = BaseScreensController.screenListRepID;
                        final ListReportController controller
                                                = (ListReportController) myController.loadScreen(name, BaseScreensController.screenListRepFile);
                        controller.initAttr(itf);
                        myController.setScreen(name);
                    }
                } catch (Exception e) {
                    Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        });
        GridPane.setMargin(this.btnRep, new Insets(0, 10, 0, 10));
        GridPane.setHalignment(this.btnRep, HPos.CENTER);
        this._mComponent.add(this.btnRep, 6, 0, 1, 1);
        
        DropShadow dropShadow = new DropShadow();
        dropShadow.setBlurType(BlurType.ONE_PASS_BOX);
        dropShadow.setColor(Color.valueOf("#0000005a"));
        this._mComponent.setEffect(dropShadow);
    }

    @Override
    protected void updateItem(Base item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            setGraphic(null);
        } else if (item != null) {
            this.chCheck.setSelected(item.isMark());
            this.lblName.setText(item.getLabel() + " >>> " + item.getCompany());
            updateKolRep(item);
            setGraphic(this._mComponent);
        }
    }
    
    public void setScreenParent(BaseScreensController screenParent) {
        this.myController = screenParent;
    }
    
    private void updateKolRep(Base mItf) {
        int reports = mItf.getReports();
        txtKolRep.setText(String.valueOf(reports));
        if (reports == 0) {
            txtKolRep.setStyle("-fx-text-fill: \"#D2691E\";");
        } else {
            txtKolRep.setStyle("-fx-text-fill: \"#008000\";");
        }
    }
}
