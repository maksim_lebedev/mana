package cl.itf.mana.client.ui;

/**
 * @author max
 * @param <T>
 */
public interface IScreens<T> extends IScreenLanguage {
    
    public void setScreenParent(T screenParent);
}
