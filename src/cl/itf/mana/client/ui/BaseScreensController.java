package cl.itf.mana.client.ui;

import cl.itf.mana.client.Mana;
import cl.itf.mana.client.model.Base;
import cl.itf.mana.client.model.Report;
import cl.itf.mana.client.util.ManaUtil;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * @author max
 */
public class BaseScreensController extends ScreensController {

    public static final String FILE_BASE_NAME = "table.json";
    
    public static String screenBaseTbID = "baseToolBar";
    public static String screenBaseTbFile = "BasesToolBar.fxml";
    public static String screenRepTbID = "repToolBar";
    public static String screenRepTbFile = "RepToolBar.fxml";
    
    public static String screenEmptyID = "empty";
    public static String screenEmptyFile = "StartEmpty.fxml";
    public static String screenAddBaseID = "addbase";
    public static String screenAddBaseFile = "AddBase.fxml";
    public static String screenAddRepID = "addreport";
    public static String screenAddRepFile = "AddReport.fxml";
    public static String screenRepRowID = "reportrow";
    public static String screenRepRowFile = "ReportRow.fxml";
    public static String screenListBaseID = "listbase";
    public static String screenListBaseFile = "ListBase.fxml";
    public static String screenEmptyRepID = "emptyrep";
    public static String screenEmptyRepFile = "EmptyRep.fxml";
    public static String screenListRepID = "listrep";
    public static String screenListRepFile = "ListReport.fxml";
    
    private final HashMap<String, IScreens<BaseScreensController>> controllers;
    private final ManaUtil util;
    
    public BaseScreensController() {
        super();
        this.controllers = new HashMap<>();
        this.util = new ManaUtil();
    }
    
    //Returns the Controller with the appropriate name
    public IScreens<BaseScreensController> getController(String name) {
        return controllers.get(name);
    }
    
    //Returns the Controllers
    public HashMap<String, IScreens<BaseScreensController>> getControllers() {
        return controllers;
    }
    
    @Override
    public void loadScreens() {
        loadScreen(screenEmptyID, screenEmptyFile);
        loadScreen(screenListBaseID, screenListBaseFile);
    }
    
    @Override
    public void goBack() {
        final ListBaseController controller = 
                           (ListBaseController) getController(screenListBaseID);
        if (controller == null || controller.isEmpty()) {
            setScreen(screenEmptyID);
        } else {
            setScreen(screenListBaseID);
        }
    }
    
    public void delBase(boolean selected) {
        final ListBaseController controller = 
                           (ListBaseController) getController(screenListBaseID);
        if (controller != null) {
            controller.delBase(selected);
        }
    }
    
    public void setDateBase(boolean selected, String date1, String date2) {
        final ListBaseController controller = 
                           (ListBaseController) getController(screenListBaseID);
        if (controller != null) {
            controller.updatePeriod(selected, date1, date2);
        }
    }
    
    public void repGen(boolean selected) {
        final ListBaseController controller = 
                           (ListBaseController) getController(screenListBaseID);
        if (controller != null) {
            controller.repGen(selected);
        }
    }
    
    public void delRep(boolean selected) {
        final ListReportController repContr = 
                   (ListReportController) getControllers().get(screenListRepID);
        if (repContr != null) {
            repContr.removeItem(selected);
        }
    }
    
    public void delRep(Report repSel) {
        final ListReportController repContr = 
                   (ListReportController) getControllers().get(screenListRepID);
        if (repContr != null) {
            repContr.removeItem(repSel);
        }
    }
    
    public void setDateRep(boolean selected, String date1, String date2) {
        final ListReportController repContr = 
                   (ListReportController) getControllers().get(screenListRepID);
        if (repContr != null) {
            repContr.updatePeriod(selected, date1, date2);
        }
    }
    
    @Override
    public void updateLang() {
        for (IScreens screenControler : controllers.values()) {
            screenControler.updateLang();
        }
    }

    @Override
    public void setScreenParent(IScreens controller) {
        controller.setScreenParent(this);
    }

    @Override
    public void putController(String name, IScreens controller) {
        controllers.put(name, controller);
    }
    
    public List<Base> importChanges() {
        return util.importDB(new File(FILE_BASE_NAME));
    }
    
    public void updateChanges(ObservableList<Base> basesData) {
        final File file = new File(FILE_BASE_NAME);
        if (file.delete()) {
            util.exportDB(file, basesData);
        }
    }
    
    /**
     * @param selected
     * @param isRep
     */
    public void showDateDialog(final boolean selected, final boolean isRep) {
        final Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.initStyle(StageStyle.TRANSPARENT);
        final Locale locale = Locale.getDefault();
        final ResourceBundle resources = 
                ResourceBundle.getBundle("cl/itf/mana/client/localization/itf", 
                                         locale);
        try {
            final FXMLLoader myLoader = new FXMLLoader(getClass().getResource("SetDate.fxml"));
            final Parent loadScreen = (Parent) myLoader.load();
            final SetDateController dateControler
                                        = ((SetDateController) myLoader.getController());
            window.setTitle(resources.getString("title.set_date"));
            window.setScene(new Scene(loadScreen, 500d, 300d, Color.TRANSPARENT));
            final Stage parent = getMainApp().getStage();
            final DialogService service = new DialogService(parent, window);
            dateControler.setDialogService(service);
            dateControler.setScreenParent(this);
            dateControler.setSelected(selected);
            dateControler.setIsRep(isRep);
            service.start();
        } catch (Exception ex) {
            Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
