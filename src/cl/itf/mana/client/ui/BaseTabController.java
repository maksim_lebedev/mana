package cl.itf.mana.client.ui;

import cl.itf.mana.client.Mana;
import cl.itf.mana.client.model.Base;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Tab;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.VBox;

/**
 * @author max
 */
public class BaseTabController extends Tab implements ITabsController {

    private ManaTabPaneController tabPaneController;
    private BaseScreensController screensController;
    
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Tab tabWork;
    
    @FXML
    private VBox vCont;
    
    @FXML
    void initialize() {
        this.screensController = new BaseScreensController();
        this.screensController.setTabController(this);
        updateLang();
    }

    private void addNodes(Node... nodes) {
        vCont.getChildren().addAll(Arrays.asList(nodes));
    }
    
    /**
     * @param name
     * @param resource
     * @return
     */
    @Override
    public IScreens replaceToolBar(String name, String resource) {
        try {
            final IScreens myControler = screensController.loadScreen(name, resource);
            final ToolBar tBar = (ToolBar) screensController.getScreen(name);
            vCont.getChildren().set(0, tBar);
            return myControler;
        } catch (Exception e) {
            Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
    }
    
    /**
     * @param name
     * @param resource
     * @return
     */
    @Override
    public ToolBar addToolBar(String name, String resource) {
        try {
            screensController.loadScreen(name, resource);
            final ToolBar tBar = (ToolBar) screensController.getScreen(name);
            return tBar;
        } catch (Exception e) {
            Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
    }
    
    @Override
    public void loadScreens(String licence) {
        final List<Base> importBase = screensController.importChanges();
        final ObservableList<Base> bases = FXCollections.observableArrayList();
        if (importBase != null) {
            bases.addAll(importBase);
        }
        
        screensController.loadScreens();

        initBases(bases);
        
        if (bases.isEmpty()) {
            screensController.setScreen(BaseScreensController.screenEmptyID);
        } else {
            screensController.setScreen(BaseScreensController.screenListBaseID);
        }

        final Group base = new Group();
        base.getChildren().addAll(screensController);

        final ToolBar tBar = addToolBar(BaseScreensController.screenBaseTbID,
                                        BaseScreensController.screenBaseTbFile);

        addNodes(tBar, base);
    }

    @Override
    public void updateLang() {
        final Locale locale = Locale.getDefault();
        resources = ResourceBundle.getBundle("cl/itf/mana/client/localization/itf", locale);
        
        tabWork.setText(resources.getString("tab.base"));
        tabWork.setClosable(false);
        
        screensController.updateLang();
    }

    @Override
    public void setTabPaneController(ManaTabPaneController tabPaneController) {
        this.tabPaneController = tabPaneController;
    }

    @Override
    public ManaTabPaneController getTabPaneController() {
        return tabPaneController;
    }

    @Override
    public void closeApp() {
        final ListBaseController listController = 
                (ListBaseController) screensController.getController(BaseScreensController.screenListBaseID);
        if (listController != null) {
            screensController.updateChanges(listController.getListBases());
        }
    }
    
    private void initBases(ObservableList<Base> bases) {
        try {
            final ListBaseController listController = 
                (ListBaseController) screensController.getController(BaseScreensController.screenListBaseID);
            for (Base itf : bases) {
                listController.addItem(itf);
            }
        } catch (Exception e) {
            Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}

