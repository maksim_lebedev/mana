package cl.itf.mana.client.ui;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.effect.ColorAdjustBuilder;
import javafx.scene.effect.Effect;
import javafx.stage.Stage;

/**
 * @author max
 */
public class DialogService extends Service<Void> {

    private final Stage window;
    private final Stage parent;
    private final Effect origEffect;

    /**
     * Creates a dialog service for showing and hiding a {@linkplain Stage}
     * <p>
     * @param parent the parent {@linkplain Stage}
     * @param window the window {@linkplain Stage} that will be shown/hidden
     */
    protected DialogService(final Stage parent, final Stage window) {
        this.window = window;
        this.parent = parent;
        this.origEffect = hasParentSceneRoot() ? this.parent.getScene().getRoot().getEffect() : null;
    }

    /**
     * {@inheritDoc}
     * @return 
     */
    @Override
    protected Task<Void> createTask() {
        return window.isShowing() ? createHideTask() : createShowTask();
    }

    /**
     * @return a task that will show the service {@linkplain Stage}
     */
    protected Task<Void> createShowTask() {
        final Task<Void> showTask = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                Platform.runLater(new Runnable() {
                    public void run() {
                        if (hasParentSceneRoot()) {
                            parent.getScene().getRoot().setEffect(
                                    ColorAdjustBuilder.create().brightness(-0.5d).build());
                        }
                        window.show();
                        window.centerOnScreen();
                    }
                });
                return null;
            }
        };
        showTask.stateProperty().addListener(new ChangeListener<State>() {
            @Override
            public void changed(final ObservableValue<? extends State> observable,
                                final State oldValue, final State newValue) {
                if (newValue == State.FAILED || newValue == State.CANCELLED) {
                    Platform.runLater(createHideTask());
                }
            }
        });
        return showTask;
    }

    /**
     * @return a task that will hide the service {@linkplain Stage}
     */
    protected Task<Void> createHideTask() {
        final Task<Void> closeTask = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                window.hide();
                if (hasParentSceneRoot()) {
                    parent.getScene().getRoot().setEffect(origEffect);
                }
                window.getScene().getRoot().setDisable(false);
                return null;
            }
        };
        return closeTask;
    }

    /**
     * @return true when the parent {@linkplain Stage#getScene()} has a valid
     *         {@linkplain Scene#getRoot()}
     */
    private boolean hasParentSceneRoot() {
        return this.parent != null && this.parent.getScene() != null
               && this.parent.getScene().getRoot() != null;
    }

    /**
     * Hides the dialog used in the {@linkplain Service}
     */
    public void hide() {
        Platform.runLater(createHideTask());
    }
}
