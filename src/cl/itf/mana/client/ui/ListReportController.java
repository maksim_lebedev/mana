package cl.itf.mana.client.ui;

import cl.itf.mana.client.Mana;
import cl.itf.mana.client.model.Base;
import cl.itf.mana.client.model.Connection;
import cl.itf.mana.client.model.Report;
import java.io.File;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.DirectoryChooser;
import javafx.util.Callback;

/**
 * @author max
 */
public class ListReportController extends HBox implements IScreens<BaseScreensController> {
    
    private Base mItf;
    private BaseScreensController myController;
//    private List<Report> listRepData;
    
    private ResourceBundle resources;
    
    @FXML
    private Label lblBack;

    @FXML
    private Button btnBack;

    @FXML
    private Label lblHead;

    @FXML
    private Label lblCatalog;

    @FXML
    private Hyperlink lblCatalogInfo;

    @FXML
    private ListView<Report> listReports;

    @FXML
    private Button btnChoose;

    @FXML
    void onBack(ActionEvent event) {
        myController.getTabController().replaceToolBar(BaseScreensController.screenBaseTbID,
                                                       BaseScreensController.screenBaseTbFile);
        myController.goBack();
    }

    @FXML
    void onChoose(ActionEvent event) {
        updateCatalogInfo();
    }
    
    @FXML
    void initialize() {
        updateLang();
        
        final String imgPath = "img/return.png";
        final Image image = new Image(Mana.class.getResourceAsStream(imgPath));
        final ImageView imageView = new ImageView(image);
        
        btnBack.setGraphic(imageView);
        lblBack.setText(resources.getString("addbase.lbl.back"));
        lblCatalog.setText(resources.getString("rep.lbl.cat"));
        
        final ObservableList<Report> basesData = FXCollections.observableArrayList();
        listReports.setItems(basesData);
        listReports.setCellFactory(new Callback<ListView<Report>, ListCell<Report>>() {
            @Override
            public ListCell<Report> call(ListView<Report> list) {
                ReportRowCell repRowCell = new ReportRowCell();
                repRowCell.setScreenParent(myController);
                return repRowCell;
            }
        });
        
        lblCatalogInfo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                updateCatalogInfo();
            }
        });
    }
    
    @Override
    public void setScreenParent(BaseScreensController screenParent){
        myController = screenParent;
    }
    
    public void initAttr(Base itf) {
        try {
            mItf = itf;
            final Connection connectData = mItf.getConnectData();
            lblHead.setText(resources.getString("rep.lbl.head") + connectData.getLabel());
            lblCatalogInfo.setText(mItf.getSaveTo());
            final ObservableList<Report> basesData = FXCollections.observableArrayList(mItf.getReplist());
            listReports.setItems(basesData);
            updateBase();
        } catch (Exception e) {
            Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public void updateLang() {
        final Locale locale = Locale.getDefault();
        resources = ResourceBundle.getBundle("cl/itf/mana/client/localization/itf", locale);
        
        lblBack.setText(resources.getString("addbase.lbl.back"));
        lblCatalog.setText(resources.getString("rep.lbl.cat"));
        if (mItf != null) {
            final Connection connectData = mItf.getConnectData();
            lblHead.setText(resources.getString("rep.lbl.head") + connectData.getLabel());
        }
    }
    
    public void updateCatalogInfo() {
        final DirectoryChooser directoryChooser = new DirectoryChooser();
        File startFile = new File(lblCatalogInfo.getText());
        if (startFile.exists()) {
            directoryChooser.setInitialDirectory(startFile);
        } else {
            if (startFile.mkdirs()) {
                directoryChooser.setInitialDirectory(startFile);
            }
        }
        final File file = directoryChooser.showDialog(myController.getMainApp().getStage());
        if (file != null) {
            String saveTo = file.getAbsolutePath();
            lblCatalogInfo.setText(saveTo);
            mItf.setSaveTo(saveTo);
        }
    }
    
    public void addItem(Report item) {
        listReports.getItems().add(item);
        updateBase();
    }
    
    public void removeItem(Report item) {
        listReports.getItems().remove(item);
        updateBase();
    }
    
    public void removeItem(boolean selected) {
        final ObservableList<Report> listRepData = listReports.getItems();
        if (selected) {
            final int[] inds = new int[listRepData.size()];
            for (Report repSel : listRepData) {
                if (repSel.isMark()) {
                    int indexOf = listRepData.indexOf(repSel);
                    inds[indexOf] = 1;
                }
            }
            for (int i = inds.length; i > 0; i--) {
                int del = inds[i - 1];
                if (del == 1) {
                    listRepData.remove(i - 1);
                }
            }
        } else {
            listRepData.clear();
        }
        updateBase();
    }
    
    public void updatePeriod(boolean selected, String date1, String date2) {
        final ObservableList<Report> listRepData = listReports.getItems();
        if (selected) {
            for (Report repSel : listRepData) {
                if (repSel.isMark()) {
                    repSel.setPeriod(date1 + "/" + date2);
                }
            }
        } else {
            for (Report repSel : listRepData) {
                repSel.setPeriod(date1 + "/" + date2);
            }
        }
        final ObservableList<Report> basesData = FXCollections.observableArrayList(listRepData);
        listReports.getItems().clear();
        listReports.setItems(null);
        listReports.setItems(basesData);
        updateBase();
    }
    
    public boolean isEmpty() {
        ObservableList<Report> items = listReports.getItems();
        return items == null ? true : items.isEmpty();
    }
    
    private void updateBase() {
        mItf.setReplist(listReports.getItems());
        ListBaseController baseContr = 
            (ListBaseController) myController.getController(BaseScreensController.screenListBaseID);
        baseContr.updateItem(mItf);
    }
}
