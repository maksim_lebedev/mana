package cl.itf.mana.client.ui;

import javafx.scene.layout.GridPane;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;

/**
 * @author max
 */
public class AboutController extends GridPane implements IScreens<MenuScreensController> {
    
    private MenuScreensController screenParent;
    
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label lblInfo;

    @FXML
    private Label lblComp;

    @FXML
    private Label lblMana;

    @FXML
    private Hyperlink linkMana;

    @FXML
    private Hyperlink linkAppMana;
    
    @FXML
    void initialize() {
        updateLang();
        
        linkMana.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                screenParent.openBrowser(linkMana.getText());
            }
        });
        
        linkAppMana.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                screenParent.openBrowser(linkAppMana.getText());
            }
        });
    }

    @Override
    public void setScreenParent(MenuScreensController screenParent) {
        this.screenParent = screenParent;
    }

    @Override
    public void updateLang() {
        final Locale locale = Locale.getDefault();
        resources = ResourceBundle.getBundle("cl/itf/mana/client/localization/itf", locale);
        
        this.lblMana.setText("MANA");
        this.lblInfo.setText(resources.getString("about.info"));
        this.lblComp.setText(resources.getString("about.product"));
    }
}
