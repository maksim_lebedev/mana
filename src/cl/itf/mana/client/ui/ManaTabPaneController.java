package cl.itf.mana.client.ui;

import cl.itf.mana.client.Mana;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.WindowEvent;

/**
 * @author max
 */
public class ManaTabPaneController extends TabPane implements EventHandler<WindowEvent> {

    private Mana mainApp;
    private ObservableList<ITabsController> listTabContr;
            
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TabPane tabPane;

    @FXML
    void initialize() {
        final Locale locale = Locale.getDefault();
        resources = ResourceBundle.getBundle("cl/itf/mana/client/localization/itf", locale);
        listTabContr = FXCollections.observableArrayList();
    }

    public void setMainApp(Mana mainApp) {
        this.mainApp = mainApp;
    }
    
    public Mana getMainApp() {
        return mainApp;
    }
    
    public void addTabs(Tab tab) {
        tabPane.getTabs().add(tab);
    }
    
    public void loadScreens(String licence) {
        try {
            Tab menuTab = initMenuTab(licence);
            if (menuTab != null) {
                addTabs(menuTab);
            }
        } catch (Exception e) {
            Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void updateLang() {
        for (ITabsController tabContr : listTabContr) {
            tabContr.updateLang();
        }
    }
    
    public void closeTabs() {
        for (ITabsController tabContr : listTabContr) {
            tabContr.closeApp();
        }
    }
    
    public void addBaseTab(String licence) {
        Tab baseTab = initBaseTab(licence);
        if (baseTab != null) {
            addTabs(baseTab);
        }
    }
    
    @Override
    public void handle(WindowEvent event) {
        closeTabs();
    }
    
    /** */
    private Tab initMenuTab(String licence) {
        try {
            final Tab menuTab = loadTab("MenuTab.fxml", licence);
            return menuTab;
        } catch (Exception e) {
            Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
    }
    
    /** */
    private Tab initBaseTab(String licence) {
        try {
            final Tab baseTab = loadTab("BaseTab.fxml", licence);
            return baseTab;
        } catch (Exception e) {
            Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
    }
    
    private Tab loadTab(String resource, String licence) {
        try {
            final FXMLLoader myLoader = new FXMLLoader(getClass().getResource(resource));
            final Tab load = (Tab) myLoader.load();
            final ITabsController tabsControler = (ITabsController) myLoader.getController();
            tabsControler.setTabPaneController(this);
            tabsControler.loadScreens(licence);
            listTabContr.add(tabsControler);
            return load;
        } catch (Exception e) {
            Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
    }
}

