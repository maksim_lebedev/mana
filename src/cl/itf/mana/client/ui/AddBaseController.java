package cl.itf.mana.client.ui;

import cl.itf.mana.client.Mana;
import cl.itf.mana.client.model.Connection;
import cl.itf.mana.client.model.Base;
import cl.itf.mana.client.util.InputConstraints;
import cl.itf.mana.client.util.TaskBasedSplash;
import java.io.File;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.DirectoryChooser;

/**
 * @author max
 */
public class AddBaseController extends HBox implements IScreens<BaseScreensController>, 
                                                       TaskBasedSplash.InitCompletionHandler {

    private Base mItf;
    private boolean editMode;
    private BaseScreensController myController;
    
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btnBack;

    @FXML
    private Label lblPort;

    @FXML
    private TextField txtHost;

    @FXML
    private Label lblName1C;

    @FXML
    private Label lblHead;

    @FXML
    private Label lblPath;

    @FXML
    private RadioButton rbtnFile;

    @FXML
    private RadioButton rbtnServer;
    
    @FXML
    private TextField txtName1C;

    @FXML
    private Label lblNameBase;

    @FXML
    private TextField txtSQLadr;

    @FXML
    private Button btnAddBase;

    @FXML
    private Label lblHost;

    @FXML
    private Label lblBack;

    @FXML
    private TextField txtPort;

    @FXML
    private Button btnChoose;

    @FXML
    private Label lblSQLadr;

    @FXML
    private TextField txtNameBase;

    @FXML
    private TextField txtPass;

    @FXML
    private CheckBox chUseAuth;

    @FXML
    private Label lblPass;

    @FXML
    private Label lblUser;

    @FXML
    private TextField txtUser;
    
    @FXML
    void onBack(ActionEvent event) {
        myController.goBack();
    }

    @FXML
    void onChoose(ActionEvent event) {
        final DirectoryChooser directoryChooser = new DirectoryChooser();
        final File startFile = new File(lblPath.getText() == null ? "" : lblPath.getText());
        if (startFile.exists()) {
            directoryChooser.setInitialDirectory(startFile);
        }
        final File file = directoryChooser.showDialog(myController.getMainApp().getStage());
        if (file != null) {
            lblPath.setText(file.getAbsolutePath() + File.separator);
            rbtnFile.setSelected(true);
        }
    }

    @FXML
    void onAddBase(ActionEvent event) {
        if (rbtnServer.isSelected()) {
            lblPath.setText(null);
        }
        if (rbtnFile.isSelected()) {
            txtSQLadr.setText(null);
            txtName1C.setText(null);
        }
        
        final Connection addDb = initConnectObject();
        
        if (editMode) {
            updateBase(addDb);
        } else {
            addBase(addDb);
        }
        
        myController.setScreen(BaseScreensController.screenListBaseID);
    }

    @FXML
    void onUseAuth(ActionEvent event) {
        txtUser.setDisable(!chUseAuth.isSelected());
        txtPass.setDisable(!chUseAuth.isSelected());
    }

    @FXML
    void initialize() {
        updateLang();
        
        final ToggleGroup group = new ToggleGroup();
        rbtnFile.setToggleGroup(group);
        rbtnServer.setToggleGroup(group);
        chUseAuth.setSelected(false);
        
        txtUser.setDisable(!chUseAuth.isSelected());
        txtPass.setDisable(!chUseAuth.isSelected());
        
        rbtnFile.selectedProperty().bindBidirectional(txtSQLadr.disableProperty());
        rbtnFile.selectedProperty().bindBidirectional(txtName1C.disableProperty());
        rbtnFile.setSelected(true);
        
        rbtnServer.selectedProperty().bindBidirectional(btnChoose.disableProperty());
        
        InputConstraints.numbersOnly(txtPort);
        InputConstraints.noLeadingAndTrailingBlanks(txtHost);
        InputConstraints.noLeadingAndTrailingBlanks(txtName1C);
        InputConstraints.noLeadingAndTrailingBlanks(txtNameBase);
        InputConstraints.noLeadingAndTrailingBlanks(txtPass);
        InputConstraints.noLeadingAndTrailingBlanks(txtPort);
        InputConstraints.noLeadingAndTrailingBlanks(txtSQLadr);
        InputConstraints.noLeadingAndTrailingBlanks(txtUser);
    }
    
    @Override
    public void setScreenParent(BaseScreensController screenParent){
        myController = screenParent;
    }
    
    private void addBase(Connection addDb) {
        TaskBasedSplash task = new TaskBasedSplash();
        task.startAddBase(addDb, this);
    }
    
    private void updateBase(Connection addDb) {
        final ListBaseController listController
                                 = (ListBaseController) myController.getController(BaseScreensController.screenListBaseID);
        mItf.setConnectData(addDb);
        listController.updateItem(mItf);
    }
    
    private Connection initConnectObject() {
        final Connection addDb = new Connection();
        addDb.setLabel(txtNameBase.getText());
        addDb.setBase(txtName1C.getText());
        addDb.setFilemode(rbtnFile.isSelected());
        addDb.setFilepath(lblPath.getText());
        addDb.setRemPort(txtPort.getText());
        addDb.setRemServer(txtHost.getText());
        addDb.setServer(txtSQLadr.getText());
        addDb.setServerMode(rbtnServer.isSelected());
        addDb.setUselogin(chUseAuth.isSelected());
        addDb.setUserName(txtUser.getText());
        addDb.setPasswd(txtPass.getText());
        return addDb;
    }

    public void edit(Base itf) {
        this.mItf = itf;
        final Connection addDb = itf.getConnectData();
        txtNameBase.setText(addDb.getLabel());
        txtName1C.setText(addDb.getBase());
        rbtnFile.setSelected(addDb.isFilemode());
        lblPath.setText(addDb.getFilepath());
        txtPort.setText(addDb.getRemPort());
        txtHost.setText(addDb.getRemServer());
        txtSQLadr.setText(addDb.getServer());
        rbtnServer.setSelected(addDb.isServerMode());
        chUseAuth.setSelected(addDb.isUselogin());
        txtUser.setText(addDb.getUserName());
        txtPass.setText(addDb.getPasswd());
        editMode = true;
        txtName1C.setEditable(!editMode);
        rbtnFile.setDisable(editMode);
        txtSQLadr.setEditable(!editMode);
        rbtnServer.setDisable(editMode);
        btnAddBase.setText(resources.getString("addbase.btn.save"));
    }
    
    @Override
    public void updateLang() {
        final Locale locale = Locale.getDefault();
        resources = ResourceBundle.getBundle("cl/itf/mana/client/localization/itf", locale);
        
        final String imgPath = "img/return.png";
        final Image image = new Image(Mana.class.getResourceAsStream(imgPath));
        final ImageView imageView = new ImageView(image);
        btnBack.setGraphic(imageView);
        
        lblPort.setText(resources.getString("addbase.lbl.port"));
        lblName1C.setText(resources.getString("addbase.lbl.name1C"));
        lblHead.setText(resources.getString("addbase.lbl.head"));
        lblPath.setText(resources.getString("addbase.lbl.path"));
        rbtnFile.setText(resources.getString("addbase.ch.file"));
        rbtnServer.setText(resources.getString("addbase.ch.server"));
        lblNameBase.setText(resources.getString("addbase.lbl.namebase"));
        btnAddBase.setText(resources.getString("addbase.btn.add"));
        lblHost.setText(resources.getString("addbase.lbl.host"));
        lblBack.setText(resources.getString("addbase.lbl.back"));
        
        lblSQLadr.setText(resources.getString("addbase.lbl.sqladr"));
        
        chUseAuth.setText(resources.getString("addbase.ch.useauth"));
        lblUser.setText(resources.getString("addbase.lbl.user"));
        lblPass.setText(resources.getString("addbase.lbl.pass"));
    }

    @Override
    public void complete(Object value) {
        try {
            final ListBaseController listController = 
                (ListBaseController) myController.getController(BaseScreensController.screenListBaseID);
            final Base[] bases = (Base[]) value;
            for (Base base : bases) {
                listController.addItem(base);
            }
        } catch (Exception e) {
            Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public void failed(String error) {
        myController.showErrorDialog(resources.getString("addbase.error"), error);
    }

    @Override
    public void cancelled() {
        
    }
}
