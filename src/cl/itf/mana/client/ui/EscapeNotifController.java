package cl.itf.mana.client.ui;

import cl.itf.mana.client.Mana;
import javafx.scene.layout.VBox;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 * @author max
 */
public class EscapeNotifController extends VBox implements IScreenLanguage {
    
    private Mana mainApp;
    
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btnCancel;

    @FXML
    private Label txt3;

    @FXML
    private Label txt4;

    @FXML
    void onCancel(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    void initialize() {
        updateLang();
    }
    
    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */
    public void setMainApp(Mana mainApp) {
        this.mainApp = mainApp;
    }
    
    public Mana getMainApp() {
        return mainApp;
    }

    @Override
    public void updateLang() {
        final Locale locale = Locale.getDefault();
        resources = ResourceBundle.getBundle("cl/itf/mana/client/localization/itf", locale);
        
        this.txt3.setText(resources.getString("txt.info3"));
        this.txt4.setText(resources.getString("txt.info4"));
        
        this.btnCancel.setText(resources.getString("btn.ok"));
    }
}
