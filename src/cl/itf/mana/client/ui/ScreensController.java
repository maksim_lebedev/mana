package cl.itf.mana.client.ui;

import cl.itf.mana.client.Mana;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

/**
 * @author max
 */
public abstract class ScreensController extends StackPane implements IScreenLanguage {

    protected ITabsController tabsController;
    protected final HashMap<String, Node> screens;

    public ScreensController() {
        this.screens = new HashMap<>();
    }
    
    /** */
    public abstract void loadScreens();
    
    /** */
    public abstract void goBack();
    
    /**
     * @param controller 
     */
    public abstract void setScreenParent(IScreens controller);
    
    /**
     * @param name
     * @param controller 
     */
    public abstract void putController(String name, IScreens controller);
    
    //Add the screen to the collection
    public void addScreen(String name, Node screen) {
        screens.put(name, screen);
    }

    //Returns the Node with the appropriate name
    public Node getScreen(String name) {
        return screens.get(name);
    }

    public void setTabController(ITabsController tabsController) {
        this.tabsController = tabsController;
    }
    
    public ITabsController getTabController() {
        return this.tabsController;
    }
    
    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */
    public void setMainApp(Mana mainApp) {
        if (tabsController != null) {
            tabsController.getTabPaneController().setMainApp(mainApp);
        }
    }
    
    public Mana getMainApp() {
        if (tabsController != null) {
            return tabsController.getTabPaneController().getMainApp();
        } else {
            return null;
        }
    }
    
    public void openBrowser(final String url) {
        if (getMainApp() != null) {
            getMainApp().getHostServices().showDocument(url);
        }
    }
    
    //Loads the fxml file, add the screen to the screens collection and
    //finally injects the screenPane to the controller.
    public IScreens loadScreen(String name, String resource) {
        try {
            final FXMLLoader myLoader = new FXMLLoader(getClass().getResource(resource));
            final Parent loadScreen = (Parent) myLoader.load();
            final IScreens myScreenControler = (IScreens) myLoader.getController();
            setScreenParent(myScreenControler);
            addScreen(name, loadScreen);
            putController(name, myScreenControler);
            return myScreenControler;
        } catch (Exception e) {
            Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
    }
    
    //This method tries to displayed the screen with a predefined name.
    //First it makes sure the screen has been already loaded.  Then if there is more than
    //one screen the new screen is been added second, and then the current screen is removed.
    // If there isn't any screen being displayed, the new screen is just added to the root.
    public boolean setScreen(final String name) {       
        if (screens.get(name) != null) {   //screen loaded
            final DoubleProperty opacity = opacityProperty();
            if (!getChildren().isEmpty()) {    //if there is more than one screen
                Timeline fade = new Timeline(
                        new KeyFrame(Duration.ZERO, new KeyValue(opacity, 1.0)),
                        new KeyFrame(new Duration(300), new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent t) {
                                getChildren().remove(0);                    //remove the displayed screen
                                getChildren().add(0, screens.get(name));    //add the screen
                                Timeline fadeIn = new Timeline(
                                        new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0)),
                                        new KeyFrame(new Duration(300), new KeyValue(opacity, 1.0)));
                                fadeIn.play();
                            }
                        }, new KeyValue(opacity, 0.0)));
                fade.play();
            } else {
                setOpacity(0.0);
                getChildren().add(screens.get(name));       //no one else been displayed, then just show
                Timeline fadeIn = new Timeline(
                        new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0)),
                        new KeyFrame(new Duration(300), new KeyValue(opacity, 1.0)));
                fadeIn.play();
            }
            return true;
        } else {
            Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, null, "screen hasn't been loaded!!! \n");
            return false;
        }
    }

    //This method will remove the screen with the given name from the collection of screens
    public boolean unloadScreen(String name) {
        if (screens.remove(name) == null) {
            Logger.getLogger(Mana.class.getName()).log(Level.INFO, null, "Screen didn't exist");
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * @param message
     * @param detail
     */
    public void showErrorDialog(final String message, final String detail) {
        final Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.initStyle(StageStyle.TRANSPARENT);
        
        try {
            final FXMLLoader myLoader = new FXMLLoader(getClass().getResource("ErrorDialog.fxml"));
            final Parent loadScreen = (Parent) myLoader.load();
            final ErrorDialogController errorControler
                                        = ((ErrorDialogController) myLoader.getController());
            window.setTitle("Error");
            window.setScene(new Scene(loadScreen, 500d, 300d, Color.TRANSPARENT));
            Stage parent = getMainApp().getStage();
            final DialogService service = new DialogService(parent, window);
            errorControler.setDialogService(service);
            errorControler.setErrorMessage(message);
            errorControler.setDetail(detail);
            service.start();
        } catch (Exception ex) {
            Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

