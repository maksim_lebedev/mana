package cl.itf.mana.client.ui;

import javafx.scene.control.ToolBar;

/**
 * @author max
 */
public interface ITabsController extends IScreenLanguage {
    
    /**
     * @param tabPaneController 
     */
    public void setTabPaneController(ManaTabPaneController tabPaneController);
    
    public ManaTabPaneController getTabPaneController();
    
    public IScreens replaceToolBar(String name, String resource);
    
    public ToolBar addToolBar(String name, String resource);
    
    public void loadScreens(String licence);
    
    public void closeApp();
}
