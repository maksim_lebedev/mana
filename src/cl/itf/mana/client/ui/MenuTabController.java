package cl.itf.mana.client.ui;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * @author max
 */
public class MenuTabController extends Tab implements ITabsController {

    private ManaTabPaneController tabPaneController;
    private MenuScreensController screensController;
    
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Tab tabMenu;

    @FXML
    private HBox hAbout;
    
    @FXML
    private VBox buttons;
    
    @FXML
    private Button exit;

    @FXML
    private Button license;

    @FXML
    private Button update;

    @FXML
    private Button lang;

    @FXML
    void onExit(ActionEvent event) {
        tabPaneController.closeTabs();
        Platform.exit();
    }

    @FXML
    void onLicense(ActionEvent event) {
        screensController.setScreen(MenuScreensController.screenLicenseID);
    }

    @FXML
    void onUpdate(ActionEvent event) {

    }

    @FXML
    void onChangeLang(ActionEvent event) {
        screensController.setScreen(MenuScreensController.screenLangID);
    }

    @FXML
    void initialize() {
        screensController = new MenuScreensController();
        screensController.setTabController(this);
        updateLang();
    }

    @Override
    public void loadScreens(String licence) {
        screensController.loadScreens();
        if (licence != null) {
            LicenseController controller = (LicenseController) 
                    screensController.getController(MenuScreensController.screenLicenseID);
            if (controller == null) {
                controller = (LicenseController) 
                    screensController.loadScreen(MenuScreensController.screenLicenseID,
                                                 MenuScreensController.screenLicenseFile);
            }
            controller.register(licence);
            screensController.setScreen(MenuScreensController.screenAboutID);
        } else {
            screensController.setScreen(MenuScreensController.screenLicenseID);
        }
        
        Group base = new Group();
        base.getChildren().addAll(screensController);
        
        addFileNode(base);
    }

    @Override
    public void updateLang() {
        final Locale locale = Locale.getDefault();
        resources = ResourceBundle.getBundle("cl/itf/mana/client/localization/itf", locale);
        
        tabMenu.setText(resources.getString("tab.menu"));
        tabMenu.setClosable(false);
        
        exit.setText(resources.getString("tab.menu.exit"));
        license.setText(resources.getString("tab.menu.license"));
        update.setText(resources.getString("tab.menu.update"));
        lang.setText(resources.getString("tab.menu.lang"));
        
        screensController.updateLang();
    }

    private void addFileNode(Node node) {
        hAbout.getChildren().add(1, node);
    }

    @Override
    public void setTabPaneController(ManaTabPaneController tabPaneController) {
        this.tabPaneController = tabPaneController;
    }

    @Override
    public ManaTabPaneController getTabPaneController() {
        return tabPaneController;
    }
    
    @Override
    public IScreens replaceToolBar(String name, String resource) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ToolBar addToolBar(String name, String resource) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void closeApp() {
        LicenseController controller = (LicenseController) 
                screensController.getController(MenuScreensController.screenLicenseID);
        if (controller == null) {
            controller = (LicenseController) 
                screensController.loadScreen(MenuScreensController.screenLicenseID,
                                             MenuScreensController.screenLicenseFile);
        }
        controller.unregister();
    }
}

