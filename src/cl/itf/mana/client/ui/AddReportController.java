package cl.itf.mana.client.ui;

import cl.itf.mana.client.Mana;
import cl.itf.mana.client.model.Base;
import cl.itf.mana.client.model.Report;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

/**
 * @author max
 */
public class AddReportController extends HBox implements IScreens<BaseScreensController> {

    private Base mItf;
    private BaseScreensController myController;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label lblBack;

    @FXML
    private Button btnBack;
    
    @FXML
    private DatePicker dt1;

    @FXML
    private DatePicker dt2;

    @FXML
    private Label lblHead;

    @FXML
    private ComboBox<String> cmbRepList;

    @FXML
    private Button btnAddRep;

    @FXML
    private TextField txtNS;

    @FXML
    void onAddRep(ActionEvent event) {
        final Report repSel = initReport();
        
        ListReportController listController = 
                (ListReportController) myController.getController(BaseScreensController.screenListRepID);
        if (listController == null) {
            listController = 
                (ListReportController) myController.loadScreen(BaseScreensController.screenListRepID, 
                                                               BaseScreensController.screenListRepFile);
            listController.initAttr(mItf);
        }
        
        listController.addItem(repSel);
        myController.setScreen(BaseScreensController.screenListRepID);
    }
    
    @FXML
    void onBack(ActionEvent event) {
        ListReportController controller = 
                (ListReportController) myController.getController(BaseScreensController.screenListRepID);
        if (controller == null) {
            controller = 
                (ListReportController) myController.loadScreen(BaseScreensController.screenListRepID, 
                                                               BaseScreensController.screenListRepFile);
        }
        if (myController.getController(BaseScreensController.screenEmptyRepID) == null) {
            myController.loadScreen(BaseScreensController.screenEmptyRepID, 
                                    BaseScreensController.screenEmptyRepFile);
        }
        
        if (controller.isEmpty()) {
            myController.setScreen(BaseScreensController.screenEmptyRepID);
        } else {
            myController.setScreen(BaseScreensController.screenListRepID);
        }
    }
    
    @FXML
    void initialize() {
        updateLang();
    }
    
    @Override
    public void setScreenParent(BaseScreensController screenParent) {
        this.myController = screenParent;
    }
    
    public void initAttr(Base itf) {
        mItf = itf;
    }
    
    public void setStRepList(ObservableList<String> repList) {
        this.cmbRepList.setItems(repList);
        this.cmbRepList.getSelectionModel().select(0);
    }

    @Override
    public void updateLang() {
        final Locale locale = Locale.getDefault();
        resources = ResourceBundle.getBundle("cl/itf/mana/client/localization/itf", locale);
        
        final String imgPath = "img/return.png";
        final Image image = new Image(Mana.class.getResourceAsStream(imgPath));
        final ImageView imageView = new ImageView(image);
        this.btnBack.setGraphic(imageView);
        
        this.lblBack.setText(resources.getString("rep.lbl.back"));        
        this.lblHead.setText(resources.getString("addrep.lbl.new"));
        this.txtNS.setPromptText(resources.getString("addrep.prompt.ns"));
        this.btnAddRep.setText(resources.getString("addrep.btn.add"));
    }
    
    private Report initReport() {
        final DateTimeFormatter sdf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        final Report repSel = new Report();
        final String name = this.cmbRepList.getSelectionModel().getSelectedItem();
        repSel.setName(name);
        final String dateStrFrom = dt1.getEditor().getText();
        final String dateStrTo = dt2.getEditor().getText();
        final LocalDate dateFrom = dt1.getConverter().fromString(dateStrFrom);
        final LocalDate dateTo = dt1.getConverter().fromString(dateStrTo);
        repSel.setPeriod(dateFrom.format(sdf) + "/" + dateTo.format(sdf));
        repSel.setValue(txtNS.getText());
        repSel.setType(cmbRepList.getSelectionModel().getSelectedIndex());
        return repSel;
    }
}
