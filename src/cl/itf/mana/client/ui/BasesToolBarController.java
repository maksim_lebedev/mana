package cl.itf.mana.client.ui;

import cl.itf.mana.client.Mana;
import javafx.scene.control.ToolBar;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * @author max
 */
public class BasesToolBarController extends ToolBar implements IScreens<BaseScreensController> {
    
    private BaseScreensController screenPage;
    
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button addBase;

    @FXML
    private Button delAllBase;

    @FXML
    private Button outRepSelBase;

    @FXML
    private Label outRepLabel;

    @FXML
    private Label setPerLabel;

    @FXML
    private Button setPerAllBase;

    @FXML
    private Label delLabel;

    @FXML
    private Button setPerSelBase;

    @FXML
    private Button delSelBase;

    @FXML
    private Button outRepAllBase;

    @FXML
    void onAddBase(ActionEvent event) {
        screenPage.loadScreen(BaseScreensController.screenAddBaseID, BaseScreensController.screenAddBaseFile);
        screenPage.setScreen(BaseScreensController.screenAddBaseID);
    }

    @FXML
    void onDelSelBase(ActionEvent event) {
        this.screenPage.delBase(true);
    }

    @FXML
    void onDelAllBase(ActionEvent event) {
        this.screenPage.delBase(false);
    }

    @FXML
    void onSetPerSelBase(ActionEvent event) {
        this.screenPage.showDateDialog(true, false);
    }

    @FXML
    void onSetPerAllBase(ActionEvent event) {
        this.screenPage.showDateDialog(false, false);
    }

    @FXML
    void onOutRepSelBase(ActionEvent event) {
        this.screenPage.repGen(true);
    }

    @FXML
    void onOutRepAllBase(ActionEvent event) {
        this.screenPage.repGen(false);
    }

    @FXML
    void initialize() {
        updateLang();
        
        String imgPath = "img/add.png";
        Image image = new Image(Mana.class.getResourceAsStream(imgPath));
        ImageView imageView = new ImageView(image);
        addBase.setGraphic(imageView);
        
        imgPath = "img/remove-selected.png";
        image = new Image(Mana.class.getResourceAsStream(imgPath));
        ImageView imageDelSel = new ImageView(image);
        delSelBase.setGraphic(imageDelSel);
        
        imgPath = "img/remove-all.png";
        image = new Image(Mana.class.getResourceAsStream(imgPath));
        ImageView imageDelAll = new ImageView(image);
        delAllBase.setGraphic(imageDelAll);
        
        imgPath = "img/date-selected.png";
        image = new Image(Mana.class.getResourceAsStream(imgPath));
        ImageView imageDateSel = new ImageView(image);
        setPerSelBase.setGraphic(imageDateSel);
        
        imgPath = "img/date-all.png";
        image = new Image(Mana.class.getResourceAsStream(imgPath));
        ImageView imageDateAll = new ImageView(image);
        setPerAllBase.setGraphic(imageDateAll);
        
        imgPath = "img/process-selected.png";
        image = new Image(Mana.class.getResourceAsStream(imgPath));
        ImageView imageProcSel = new ImageView(image);
        outRepSelBase.setGraphic(imageProcSel);
        
        imgPath = "img/process-all.png";
        image = new Image(Mana.class.getResourceAsStream(imgPath));
        ImageView imageProcAll = new ImageView(image);
        outRepAllBase.setGraphic(imageProcAll);
    }

    @Override
    public void setScreenParent(BaseScreensController screenPage) {
        this.screenPage = screenPage;
    }

    @Override
    public void updateLang() {
        final Locale locale = Locale.getDefault();
        resources = ResourceBundle.getBundle("cl/itf/mana/client/localization/itf", locale);
        
        addBase.setText(resources.getString("bar.btn.addnewdb"));
        
        delLabel.setText(resources.getString("bar.lbl.del"));
        delSelBase.setText(resources.getString("bar.lbl.del.sel"));
        delAllBase.setText(resources.getString("bar.lbl.del.all"));
        
        setPerLabel.setText(resources.getString("bar.lbl.setper"));
        setPerSelBase.setText(resources.getString("bar.btn.setper.sel"));
        setPerAllBase.setText(resources.getString("bar.btn.setper.all"));
        
        outRepLabel.setText(resources.getString("bar.lbl.outrep"));
        outRepSelBase.setText(resources.getString("bar.btn.outrep.sel"));
        outRepAllBase.setText(resources.getString("bar.btn.outrep.all"));
    }
}
