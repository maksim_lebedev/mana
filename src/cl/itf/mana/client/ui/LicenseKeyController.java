package cl.itf.mana.client.ui;

import cl.itf.mana.client.Mana;
import javafx.scene.layout.VBox;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 * @author max
 */
public class LicenseKeyController extends VBox implements IScreenLanguage {
    
    private Mana mainApp;
    
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btnCancel;

    @FXML
    private Label txt2;

    @FXML
    private Label txt1;

    @FXML
    private Button btnEntKey;

    @FXML
    void onCancel(ActionEvent event) {
        mainApp.initEscapeNotifLayout();
    }

    @FXML
    void onEntKey(ActionEvent event) {
        mainApp.initTabPane(null);
    }

    @FXML
    void initialize() {
        updateLang();
    }
    
    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */
    public void setMainApp(Mana mainApp) {
        this.mainApp = mainApp;
    }
    
    public Mana getMainApp() {
        return mainApp;
    }

    @Override
    public void updateLang() {
        final Locale locale = Locale.getDefault();
        resources = ResourceBundle.getBundle("cl/itf/mana/client/localization/itf", locale);
        
        this.txt1.setText(resources.getString("txt.info1"));
        this.txt2.setText(resources.getString("txt.info2"));
        
        this.btnCancel.setText(resources.getString("btn.close"));
        this.btnEntKey.setText(resources.getString("btn.havekey"));
    }
}
