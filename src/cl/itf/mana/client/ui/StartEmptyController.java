package cl.itf.mana.client.ui;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 * @author Angie
 */
public class StartEmptyController implements Initializable, IScreens<BaseScreensController> {

    private BaseScreensController myController;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    
    @Override
    public void setScreenParent(BaseScreensController screenParent){
        myController = screenParent;
    }

    @Override
    public void updateLang() {
        
    }
}
