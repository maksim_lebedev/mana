package cl.itf.mana.client.ui;

import java.util.HashMap;

/**
 * @author max
 */
public class MenuScreensController extends ScreensController {

    public static String screenAboutID = "about";
    public static String screenAboutFile = "About.fxml";
    public static String screenLangID = "lang";
    public static String screenLangFile = "Lang.fxml";
    public static String screenLicenseID = "license";
    public static String screenLicenseFile = "License.fxml";
    
    private final HashMap<String, IScreens<MenuScreensController>> controllers;

    public MenuScreensController() {
        super();
        this.controllers = new HashMap<>();
    }
    
    //Returns the Controller with the appropriate name
    public IScreens getController(String name) {
        return controllers.get(name);
    }
    
    //Returns the Controllers
    public HashMap<String, IScreens<MenuScreensController>> getControllers() {
        return controllers;
    }
    
    @Override
    public void loadScreens() {
        super.loadScreen(screenAboutID, screenAboutFile);
        super.loadScreen(screenLicenseID, screenLicenseFile);
        super.loadScreen(screenLangID, screenLangFile);
    }

    @Override
    public void goBack() {
        
    }
    
    @Override
    public void updateLang() {
        if (controllers != null) {
            for (IScreenLanguage screenControler : controllers.values()) {
                screenControler.updateLang();
            }
        }
    }

    @Override
    public void setScreenParent(IScreens controller) {
        controller.setScreenParent(this);
    }

    @Override
    public void putController(String name, IScreens controller) {
        controllers.put(name, controller);
    }
}
