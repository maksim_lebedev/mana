package cl.itf.mana.client.ui;

import cl.itf.mana.client.Mana;
import cl.itf.mana.client.model.Base;
import javafx.scene.control.ToolBar;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * @author max
 */
public class RepToolBarController extends ToolBar implements IScreens<BaseScreensController> {

    private final ObservableList<String> repList
            = FXCollections.observableArrayList("Оборотно Сальдовая Ведомость",
                                                "Оборотно Сальдовая Ведомость По Счету",
                                                "Карточка Счета",
                                                "Анализ Счета",
                                                "Анализ Счета По Субконто",
                                                "Анализ Субконто",
                                                "Отчет По Проводкам");
    private Base mItf;
    private BaseScreensController screenPage;
    
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label delLabelRep;

    @FXML
    private Button delAllRep;

    @FXML
    private Button delSelRep;

    @FXML
    private Button addRep;

    @FXML
    private Button setPerAllRep;

    @FXML
    private Label setPerLabelRep;

    @FXML
    private Button setPerSelRep;

    @FXML
    void onAddRep(ActionEvent event) {
        AddReportController controller = 
            (AddReportController) screenPage.loadScreen(BaseScreensController.screenAddRepID, 
                                                        BaseScreensController.screenAddRepFile);
        controller.setStRepList(repList);
        controller.initAttr(mItf);
        screenPage.setScreen(BaseScreensController.screenAddRepID);
    }

    @FXML
    void onDelSelRep(ActionEvent event) {
        this.screenPage.delRep(true);
    }

    @FXML
    void onDelAllRep(ActionEvent event) {
        this.screenPage.delRep(false);
    }

    @FXML
    void onSetPerSelRep(ActionEvent event) {
        this.screenPage.showDateDialog(true, true);
    }

    @FXML
    void onSetPerAllRep(ActionEvent event) {
        this.screenPage.showDateDialog(false, true);
    }
    
    @FXML
    void initialize() {
        updateLang();
        
        String imgPath = "img/add.png";
        Image image = new Image(Mana.class.getResourceAsStream(imgPath));
        ImageView imageView = new ImageView(image);
        addRep.setGraphic(imageView);
        
        imgPath = "img/remove-selected.png";
        image = new Image(Mana.class.getResourceAsStream(imgPath));
        ImageView imageDelSel = new ImageView(image);
        delSelRep.setGraphic(imageDelSel);
        
        imgPath = "img/remove-all.png";
        image = new Image(Mana.class.getResourceAsStream(imgPath));
        ImageView imageDelAll = new ImageView(image);
        delAllRep.setGraphic(imageDelAll);
        
        imgPath = "img/date-selected.png";
        image = new Image(Mana.class.getResourceAsStream(imgPath));
        ImageView imageDateSel = new ImageView(image);
        setPerSelRep.setGraphic(imageDateSel);
        
        imgPath = "img/date-all.png";
        image = new Image(Mana.class.getResourceAsStream(imgPath));
        ImageView imageDateAll = new ImageView(image);
        setPerAllRep.setGraphic(imageDateAll);
    }
    
    @Override
    public void setScreenParent(BaseScreensController screenPage) {
        this.screenPage = screenPage;
    }
    
    public ObservableList<String> getStRepList() {
        return repList;
    }

    public void initAttr(Base itf) {
        mItf = itf;
    }
         
    @Override
    public void updateLang() {
        final Locale locale = Locale.getDefault();
        resources = ResourceBundle.getBundle("cl/itf/mana/client/localization/itf", locale);
        
        addRep.setText(resources.getString("bar.btn.addnewrep"));
        
        delLabelRep.setText(resources.getString("bar.lbl.del.rep"));
        delSelRep.setText(resources.getString("bar.lbl.del.rep.sel"));
        delAllRep.setText(resources.getString("bar.lbl.del.rep.all"));
        
        setPerLabelRep.setText(resources.getString("bar.lbl.setper.rep"));
        setPerSelRep.setText(resources.getString("bar.btn.setper.rep.sel"));
        setPerAllRep.setText(resources.getString("bar.btn.setper.rep.all"));
    }
}
