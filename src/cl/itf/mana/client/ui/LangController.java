package cl.itf.mana.client.ui;

import javafx.scene.layout.GridPane;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;

/**
 * @author max
 */
public class LangController extends GridPane implements IScreens<MenuScreensController> {

    private MenuScreensController screenParent;
    
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btnSave;

    @FXML
    private Label lblLang;

    @FXML
    private Label lblHead;

    @FXML
    private CheckBox chRus;

    @FXML
    private CheckBox chEng;

    @FXML
    void onRu(ActionEvent event) {
        chEng.setSelected(false);
    }

    @FXML
    void onEng(ActionEvent event) {
        chRus.setSelected(false);
    }
    
    @FXML
    void onSave(ActionEvent event) {
        if (chRus.isSelected()) {
            Locale rus = new Locale("ru");
            Locale.setDefault(rus);
        } else {
            Locale eng = Locale.ENGLISH;
            Locale.setDefault(eng);
        }
        
        screenParent.getTabController().getTabPaneController().updateLang();
    }

    @FXML
    void initialize() {
        updateLang();
    }
    
    @Override
    public void setScreenParent(MenuScreensController screenParent) {
        this.screenParent = screenParent;
    }

    @Override
    public void updateLang() {
        final Locale locale = Locale.getDefault();
        resources = ResourceBundle.getBundle("cl/itf/mana/client/localization/itf", locale);
        
        this.lblHead.setText(resources.getString("lang.head"));
        this.lblLang.setText(resources.getString("lang.lang"));
        
        this.chRus.setText(resources.getString("lang.ru"));
        this.chEng.setText(resources.getString("lang.en"));
        
        this.btnSave.setText(resources.getString("lang.btn.save"));
        
        if (locale.getLanguage().isEmpty() || locale.getLanguage().contains("ru")) {
            this.chRus.setSelected(true);
        } else {
            this.chEng.setSelected(true);
        }
    }
}
