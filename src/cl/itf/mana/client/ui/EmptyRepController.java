package cl.itf.mana.client.ui;

import cl.itf.mana.client.Mana;
import cl.itf.mana.client.model.Base;
import cl.itf.mana.client.model.Report;
import cl.itf.mana.client.util.ManaUtil;
import javafx.scene.layout.HBox;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * @author sniffl
 */
public class EmptyRepController extends HBox implements IScreens<BaseScreensController> {

    private Base mItf;
    private ObservableList<String> repList;
    private BaseScreensController myController;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label lblBack;

    @FXML
    private Button btnBack;

    @FXML
    private Button btnAddStRep;

    @FXML
    private Label lblQuest;

    @FXML
    private Label lblTx;

    @FXML
    private Button btnAddRep;

    @FXML
    void onBack(ActionEvent event) {
        myController.getTabController().replaceToolBar(BaseScreensController.screenBaseTbID,
                                                       BaseScreensController.screenBaseTbFile);
        myController.goBack();
    }

    @FXML
    void onAddRep(ActionEvent event) {
        AddReportController controller = 
            (AddReportController) myController.loadScreen(BaseScreensController.screenAddRepID, 
                                                          BaseScreensController.screenAddRepFile);
        controller.setStRepList(repList);
        final ListReportController repcontroller
                = (ListReportController) myController.loadScreen(BaseScreensController.screenListRepID, 
                                                              BaseScreensController.screenListRepFile);
        repcontroller.initAttr(mItf);
        myController.setScreen(BaseScreensController.screenAddRepID);
    }

    @FXML
    void onAddStRep(ActionEvent event) {
        final List<Report> repSelList = new ArrayList<>(repList.size());
        int i = 0;
        for (String rep : repList) {
            final Report repS = new Report();
            repS.setName(rep);
            repS.setPeriod(ManaUtil.getKvartal());
            repS.setMark(false);
            repS.setType(i);
            repSelList.add(repS);
            i++;
        }//end for
        mItf.setReplist(repSelList);
        final ListReportController controller
                = (ListReportController) myController.loadScreen(BaseScreensController.screenListRepID, 
                                                              BaseScreensController.screenListRepFile);
        controller.initAttr(mItf);
        myController.setScreen(BaseScreensController.screenListRepID);
    }

    @FXML
    void initialize() {
        updateLang();
        
        final String imgPath = "img/return.png";
        final Image image = new Image(Mana.class.getResourceAsStream(imgPath));
        final ImageView imageView = new ImageView(image);
        this.btnBack.setGraphic(imageView);
    }
    
    @Override
    public void setScreenParent(BaseScreensController screenParent) {
        this.myController = screenParent;
    }
    
    public void setStRepList(ObservableList<String> repList) {
        this.repList = repList;
    }
    
    public void initAttr(Base itf) {
        this.mItf = itf;
    }

    @Override
    public void updateLang() {
        final Locale locale = Locale.getDefault();
        resources = ResourceBundle.getBundle("cl/itf/mana/client/localization/itf", locale);
        
        this.lblBack.setText(resources.getString("addbase.lbl.back"));        
        this.btnAddStRep.setText(resources.getString("repempty.btn.yes"));
        this.lblQuest.setText(resources.getString("repempty.lbl.quest"));
        this.lblTx.setText(resources.getString("repempty.lbl.tx"));
        this.btnAddRep.setText(resources.getString("repempty.lbl.addhand"));
    }
}
