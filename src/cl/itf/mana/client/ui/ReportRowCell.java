package cl.itf.mana.client.ui;

import cl.itf.mana.client.Mana;
import cl.itf.mana.client.model.Report;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;

/**
 * @author max
 */
public class ReportRowCell extends ListCell<Report> {
    
    private BaseScreensController myController;
    private final ResourceBundle resources;
    private final GridPane _mComponent;
    private final CheckBox chCheck;
    private final Label lblName;
    private final Label lblD1;
    private final Label lblD2;
    private final Button btnDel;

    public ReportRowCell() {
        final Locale locale = Locale.getDefault();
        resources = ResourceBundle.getBundle("cl/itf/mana/client/localization/itf", locale);
        
        this._mComponent = new GridPane();
        this._mComponent.getStyleClass().add("repgrid");
        
        RowConstraints rowConstraints1 = new RowConstraints();
        rowConstraints1.setVgrow(Priority.SOMETIMES);
        rowConstraints1.setMinHeight(35.0);
        rowConstraints1.setPrefHeight(35.0);
        
        RowConstraints rowConstraints2 = new RowConstraints();
        rowConstraints2.setVgrow(Priority.SOMETIMES);
        rowConstraints2.setMinHeight(35.0);
        rowConstraints2.setPrefHeight(35.0);
        
        ColumnConstraints c1 = new ColumnConstraints();
        c1.setHgrow(Priority.NEVER);
        c1.setMinWidth(50.0);
        c1.setPrefWidth(50.0);
        c1.setMaxWidth(50.0);
        
        ColumnConstraints c2 = new ColumnConstraints();
        c2.setHgrow(Priority.ALWAYS);
        
        ColumnConstraints c3 = new ColumnConstraints();
        c3.setHgrow(Priority.ALWAYS);
        
        ColumnConstraints c4 = new ColumnConstraints();
        c4.setHgrow(Priority.SOMETIMES);
        
        ColumnConstraints c5 = new ColumnConstraints();
        c5.setHgrow(Priority.NEVER);
        
        this._mComponent.getRowConstraints().addAll(rowConstraints1, rowConstraints2);
        this._mComponent.getColumnConstraints().addAll(c1, c2, c3, c4, c5);
        
        this.chCheck = new CheckBox();
        this.chCheck.setMnemonicParsing(false);
        this.chCheck.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Report item = getItem();
                CheckBox source = (CheckBox) event.getSource();
                item.setMark(source.isSelected());
            }
        });
        GridPane.setMargin(this.chCheck, new Insets(0, 10, 0, 10));
        this._mComponent.add(this.chCheck, 0, 0, 1, 2);
        
        this.lblName = new Label();
        this.lblName.setWrapText(true);
        this.lblName.getStyleClass().add("label-bright");
        GridPane.setHalignment(this.lblName, HPos.LEFT);
        this._mComponent.add(this.lblName, 1, 0, 2, 2);

        this.lblD1 = new Label();
        this.lblD1.getStyleClass().add("label-bright");
        GridPane.setHalignment(this.lblD1, HPos.LEFT);
        this._mComponent.add(this.lblD1, 3, 0);
        
        this.lblD2 = new Label();
        this.lblD2.getStyleClass().add("label-bright");
        GridPane.setHalignment(this.lblD2, HPos.LEFT);
        this._mComponent.add(this.lblD2, 3, 1);
        
        this.btnDel = new Button();
        this.btnDel.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        this.btnDel.setMnemonicParsing(false);
        this.btnDel.setMaxHeight(50.0);
        this.btnDel.setMaxWidth(50.0);
        this.btnDel.setMinHeight(50.0);
        this.btnDel.setMinWidth(50.0);
        this.btnDel.setPrefHeight(50.0);
        this.btnDel.setPrefWidth(50.0);
        String imgPath = "img/remove.png";
        Image image = new Image(Mana.class.getResourceAsStream(imgPath));
        final ImageView imageView = new ImageView(image);
        this.btnDel.setGraphic(imageView);
        this.btnDel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Report item = getItem();
                myController.delRep(item);
            }
        });
        GridPane.setMargin(this.btnDel, new Insets(0, 10, 0, 10));
        GridPane.setHalignment(this.btnDel, HPos.CENTER);
        this._mComponent.add(this.btnDel, 4, 0, 1, 2);
    }

    @Override
    protected void updateItem(Report item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            setGraphic(null);
        } else if (item != null) {
            this.chCheck.setSelected(item.isMark());
            final String value = item.getValue();
            if (value != null && !"null".equals(value)) {
                this.lblName.setText(item.getName() + " " + value);
            } else {
                this.lblName.setText(item.getName());
            }
            final String period = item.getPeriod();
            if (period == null || period.isEmpty()) {
                this.lblD1.setText(null);
                this.lblD2.setText(null);
            } else {
                final String[] split = period.split("/");
                if (split.length > 1) {
                    this.lblD1.setText(split[0]);
                    this.lblD2.setText(split[1]);
                } else if (split.length == 1) {
                    this.lblD1.setText(split[0]);
                    this.lblD2.setText(null);
                } else {
                    this.lblD1.setText(null);
                    this.lblD2.setText(null);
                }
            }
            setGraphic(this._mComponent);
        }
    }
    
    public void setScreenParent(BaseScreensController screenParent) {
        this.myController = screenParent;
    }
}
