package cl.itf.mana.client.ui;

/**
 * @author max
 */
public interface IScreenLanguage {
    
    public void updateLang();
}
