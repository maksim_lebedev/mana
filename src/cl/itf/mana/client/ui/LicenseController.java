package cl.itf.mana.client.ui;

import cl.itf.mana.client.Mana;
import cl.itf.mana.client.model.License;
import cl.itf.mana.client.util.TaskBasedSplash;
import cl.itf.mana.client.util.TaskBasedSplash.InitCompletionHandler;
import com.google.gson.Gson;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import javafx.scene.layout.GridPane;
import java.net.URL;
import java.security.MessageDigest;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * @author max
 */
public class LicenseController extends GridPane implements IScreens<MenuScreensController>, 
                                                           InitCompletionHandler {
    
    private static final int BUFFER_SIZE = 1024;
    private MenuScreensController screenParent;
    private String mLicense;
    
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label lblLicInfo;

    @FXML
    private Label lblLicense;

    @FXML
    private TextField txtKey;
    
    @FXML
    private Button btnRegister;

    @FXML
    void onRegister(ActionEvent event) {
        try {
            if (this.txtKey.getText().isEmpty()) {
                screenParent.showErrorDialog(resources.getString("lic.error"), 
                                             "Некорректно введен ключ");
            } else {
                MessageDigest md = MessageDigest.getInstance("MD5");
                md.reset();
                final String plaintext = txtKey.getText();
                md.update(plaintext.getBytes());
                byte[] digest = md.digest();
                BigInteger bigInt = new BigInteger(1, digest);
                String key = bigInt.toString(16);
                // Now we need to zero pad it if you actually want the full 32 chars.
                while (key.length() < 32) {
                    key = "0" + key;
                }
                
                register(key);
            }
        } catch (Exception ex) {
            Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, null, ex);
            screenParent.showErrorDialog(resources.getString("lic.error"), 
                                         ex.getMessage());
        }//end try
    }

    @FXML
    void initialize() {
        updateLang();
        this.lblLicInfo.setText(null);
        this.txtKey.setOnKeyPressed((KeyEvent event) -> {
            if (event.getCode() == KeyCode.ENTER) {
                onRegister(null);
                event.consume();
            }
        });
    }
    
    @Override
    public void setScreenParent(MenuScreensController screenParent) {
        this.screenParent = screenParent;
    }

    @Override
    public void updateLang() {
        final Locale locale = Locale.getDefault();
        
        String licText = null;
        if (resources != null) {
             licText = resources.getString("lic.reg");
        }
        resources = ResourceBundle.getBundle("cl/itf/mana/client/localization/itf", locale);
        
        this.lblLicense.setText(resources.getString("lic.head"));
        
        String licInfo = this.lblLicInfo.getText();
        if (licText != null && licInfo != null) {
            int indexOf = licInfo.lastIndexOf(licText);
            if (indexOf >= 0) {
                licInfo = licInfo.substring(indexOf + licText.length());
                this.lblLicInfo.setText(resources.getString("lic.reg") + licInfo);
            }
        }
                
        this.txtKey.setPromptText(resources.getString("lic.prompt"));
        this.btnRegister.setText(resources.getString("lic.btn.registr"));
    }

    public void register(String license) {
        this.mLicense = license;
        String url = "http://mls.appmana.ru/keys/register/?key=" + license;
        startZapros(url);
    }
    
    public void unregister() {
        if (this.mLicense != null) {
            String url = "http://mls.appmana.ru/keys/unregister/?key=" + this.mLicense;
            startZapros(url);
        }
    }
    
    @Override
    public void complete(Object value) {
        final Gson gson = new Gson();
        String licensejson = (String) value;
        final License license = gson.fromJson(licensejson, License.class);
        if (license.isRegistered()) {
            final String newString = encode(license.getOwner());
            this.lblLicInfo.setText(resources.getString("lic.reg") 
                                    + (newString == null ? license.getOwner() : newString));
            this.txtKey.setVisible(false);
            this.btnRegister.setVisible(false);
            screenParent.getTabController().getTabPaneController().addBaseTab(this.mLicense);
            
            final File keyFile = new File("key.license");
            try (FileOutputStream fos = new FileOutputStream(keyFile);
                 BufferedOutputStream dest = new BufferedOutputStream(fos, BUFFER_SIZE)) {
                byte data[] = this.mLicense.getBytes();
                dest.write(data, 0, data.length);
                dest.flush();
            } catch (IOException ex) {
                Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            this.mLicense = null;
            if (!"unregistered".equals(license.getResponse())) {
                final String newString = encode(license.getStatus());
                screenParent.showErrorDialog(resources.getString("lic.error"),
                                             newString == null ? license.getStatus() : newString);
            }
        }
    }

    @Override
    public void failed(String error) {
        this.mLicense = null;
        final String newString = encode(error);
        screenParent.showErrorDialog(resources.getString("lic.error"), 
                                     newString == null ? error : newString);
    }

    @Override
    public void cancelled() {
        this.mLicense = null;
    }
    
    private void startZapros(String zapros) {
        TaskBasedSplash task = new TaskBasedSplash();
        task.startLicense(zapros, this);
    }
    
    private String encode(String oldString) {
        String newString = null;
        if (oldString != null) {
            try {
                newString = new String(oldString.getBytes(), "UTF-8");
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(LicenseController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return newString;
    }
}
