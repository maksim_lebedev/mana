package cl.itf.mana.client.ui;

import javafx.scene.layout.GridPane;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;

/**
 * @author max
 */
public class SetDateController extends GridPane {
    
    private boolean selected;
    private boolean isRep;
    private DialogService service;
    private BaseScreensController myController;
    
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private DatePicker dt1;

    @FXML
    private DatePicker dt2;

    @FXML
    private Label lblHead;

    @FXML
    private Button btnSet;

    @FXML
    private Button btnCancel;
    
    @FXML
    void onSet(ActionEvent event) {
        final DateTimeFormatter formater = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        
        String date = dt1.getEditor().getText();
        LocalDate localD = dt1.getConverter().fromString(date);
        final String date1 = localD.format(formater);
        
        date = dt2.getEditor().getText();
        localD = dt2.getConverter().fromString(date);
        final String date2 = localD.format(formater);
        
        if (isRep) {
            this.myController.setDateRep(selected, date1, date2);
        } else {
            this.myController.setDateBase(selected, date1, date2);
        }
        this.service.hide();
    }

    @FXML
    void onCancel(ActionEvent event) {
        this.service.hide();
    }
    
    @FXML
    void initialize() {
        final Locale locale = Locale.getDefault();
        resources = ResourceBundle.getBundle("cl/itf/mana/client/localization/itf", locale);
        
        this.lblHead.setText(resources.getString("bar.lbl.setper"));
        this.btnSet.setText(resources.getString("btn.set"));
        this.btnCancel.setText(resources.getString("btn.cancel"));
    }
    
    public void setSelected(boolean selected) {
        this.selected = selected;
    }
    
    public void setIsRep(boolean isRep) {
        this.isRep = isRep;
    }
    
    public void setDialogService(DialogService service) {
        this.service = service;
    }
    
    public void setScreenParent(BaseScreensController screenParent){
        this.myController = screenParent;
    }
}
