package cl.itf.mana.client.util;

import cl.itf.j1c.chatmessage.ChatMessage;
import cl.itf.mana.client.Mana;
import cl.itf.mana.client.model.Connection;
import cl.itf.mana.client.model.Base;
import cl.itf.mana.client.model.Report;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.*;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.*;
import javafx.stage.*;
import javafx.util.Duration;

/**
 * @author sniffl
 */
public class TaskBasedSplash {

    private final ResourceBundle resources;
    private final Pane splashLayout;
    private final ProgressBar loadProgress;
    private final TextArea progressTextArea;
    private final Button progressStop;
    private static final int SPLASH_WIDTH = 676;
    private static final int SPLASH_HEIGHT = 227;

    public TaskBasedSplash() {
        final Locale locale = Locale.getDefault();
        resources = ResourceBundle.getBundle("cl/itf/mana/client/localization/itf", locale);
        
        loadProgress = new ProgressBar();
        loadProgress.setPrefWidth(SPLASH_WIDTH - 20);
        loadProgress.setPadding(new Insets(15, 5, 15, 5));
        
        progressTextArea = new TextArea();
        progressTextArea.setWrapText(true);
        
        progressStop = new Button(resources.getString("btn.cancel"));
        progressStop.setAlignment(Pos.BASELINE_RIGHT);
        progressStop.setCancelButton(true);
        progressStop.setId("buttonBase");
        
        splashLayout = new VBox();
        final HBox hBox = new HBox();
        hBox.getChildren().addAll(loadProgress, progressStop);
        splashLayout.getChildren().addAll(hBox, progressTextArea);
        splashLayout.getStylesheets().add("cl/itf/mana/client/manapp.css");
        splashLayout.setStyle(
                  "-fx-padding: 5; "
                + "-fx-background-color: cornsilk; "
                + "-fx-border-width:1; "
                + "-fx-border-color: "
                + "linear-gradient("
                + "to bottom, "
                + "chocolate, "
                + "derive(chocolate, 50%)"
                + ");"
        );
        splashLayout.setEffect(new DropShadow());
    }

    public void startLicense(String zapros, InitCompletionHandler handler) {
        final Task<String> friendTask = new Task<String>() {
            @Override
            protected String call() throws Exception {
                updateMessage("Start check license . . . \n");
                final URL url = new URL(zapros);
                updateMessage("Start check license . . . \n");
                String json = null;
                try {
                    final HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    updateProgress(ProgressBar.INDETERMINATE_PROGRESS, 1000);
                    con.connect();

                    updateProgress(1000, 1000);

                    final InputStream inputStream = con.getInputStream();

                    try (BufferedReader in = new BufferedReader(new InputStreamReader(inputStream))) {
                        final StringBuilder response = new StringBuilder();
                        String inputLine;
                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }

                        json = response.toString();
                    }
                } catch (SocketTimeoutException ex) {
                    throw new IOException("Истекло время ожидания: " + ex.getMessage());
                } catch (UnknownHostException ex) {
                    throw new IOException("Запрашиваемый адрес " + ex.getMessage() + " недоступен");
                } catch (Exception ex) {
                    throw new IOException(ex.getMessage());
                }
                
                return json;
            }

            @Override
            protected void cancelled() {
                super.cancelled();
                updateMessage("Check license . . . canceled \n");
            }
        };

        final Stage initStage = new Stage();
        showSplash(initStage, friendTask, handler);
        new Thread(friendTask).start();
    }

    public void startAddBase(Connection data, InitCompletionHandler handler) {
        final Task<Base[]> friendTask = new Task<Base[]>() {
            @Override
            protected Base[] call() throws Exception {
                int current = 1000 / 2;
                String connect = getConnectStr(data);
                updateMessage("Connect to server . . . \n");
                updateProgress(current, 1000);
                String company = getCompany(data.getRemServer(),
                                            data.getRemPort(),
                                            connect,
                                            data.getUserName(),
                                            data.getPasswd());
                current = 1000;
                updateProgress(current, 1000);
                
                Base[] row = new Base[]{};
                if (company != null && !company.isEmpty()) {
                    String[] comname = company.split(";");
                    row = new Base[comname.length];
                    final String curDir = System.getProperty("user.dir") 
                                        + File.separator;
                    for (int i = 0; i < comname.length; i++) {
                        final Base base = new Base();
                        base.setMark(true);
                        base.setLabel(data.getLabel());
                        base.setPath(connect);
                        base.setCompany(comname[i]);
                        base.setReports(0);
                        base.setSaveTo(curDir + data.getLabel() + File.separator);
                        base.setConnectData(data);
                        row[i] = base;
                    }
                }//end if

                return row;
            }

            @Override
            protected void cancelled() {
                super.cancelled();
                updateMessage("Add base . . . canceled \n");
            }
        };

        final Stage initStage = new Stage();
        showSplash(initStage, friendTask, handler);
        progressTextArea.appendText("Try add base . . . \n");
        new Thread(friendTask).start();
    }
    
    public void startRepGen(final List<Base> rows, final boolean select, InitCompletionHandler handler) {
        final Task<String> friendTask = new Task<String>() {
            @Override
            protected String call() throws Exception {
                int current = 1;
                String res = null;
                for (Base itf : rows) {
                    if (select && !itf.isMark()) {
                        continue;
                    }

                    Connection connectData = itf.getConnectData();
                    current++;
                    updateProgress(current / 2, rows.size());
                    String server = connectData.getRemServer();
                    String port = connectData.getRemPort();
                    List<Report> replist = itf.getReplist();
                    StringBuilder sbd = new StringBuilder();
                    int i = 0;
                    for (Report rs : replist) {
                        if (i != 0) {
                            sbd.append("#");
                        }
                        sbd.append(rs.getName()).append(";");
                        sbd.append(rs.getPeriod()).append(";");
                        sbd.append(rs.getValue()).append(";");
                        sbd.append(rs.isMark()).append(";");
                        sbd.append(rs.getType()).append(";");
                        i++;
                    }//end for
                    String reports = sbd.toString();
                    long d1 = System.currentTimeMillis();
                    String username = "J1CREPQ" + d1;

                    Client client = new Client(server, Integer.parseInt(port), username);
                    updateMessage("Base " + itf.getLabel() + " - connect to server . . . \n");
                    if (client.connect()) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(ChatMessage.REPGENIND).append(ChatMessage.delim);
                        sb.append(itf.getPath()).append(ChatMessage.delim);
                        sb.append(connectData.getUserName() == null ? "" : connectData.getUserName()).append(ChatMessage.delim);
                        sb.append(connectData.getPasswd() == null ? "" : connectData.getPasswd()).append(ChatMessage.delim);
                        sb.append(itf.getSaveTo()).append(ChatMessage.delim);
                        sb.append(itf.getCompany()).append(ChatMessage.delim);
                        sb.append(itf.getLabel()).append(ChatMessage.delim);
                        sb.append(reports).append(ChatMessage.delim);

                        updateMessage("Base " + itf.getLabel() + " - exchange messages . . . \n");
                        client.messageExchange(sb.toString());

                        ChatMessage msg;
                        while ((msg = client.getMessage()) == null) {

                        }
                        client.disconnect();
                        client.stop();
                        
                        // the messaage part of the ChatMessage
                        String message = msg.getMessage();
                        updateMessage("Base " + itf.getLabel() + " - get message " + message + " \n");

                        // Switch on the type of message receive
                        switch (msg.getType()) {
                            case ChatMessage.RESULT:
                                res = message;
                                break;
                            case ChatMessage.LOGOUT:
                                res = null;
                                break;
                            case ChatMessage.ERROR:
                                res = message;
                                throw new Exception(res);
                        }//end switch                        
                    }//end if
                }//end for

                updateMessage("Generate reports finish \n");
                updateProgress(rows.size(), rows.size());
                return res;
            }
            
            @Override
            protected void cancelled() {
                super.cancelled();
                updateMessage("Generate reports . . . canceled \n");
            }
        };
        
        final Stage initStage = new Stage();
        showSplash(initStage, friendTask, handler);
        progressTextArea.appendText("Generate reports . . . \n");
        new Thread(friendTask).start();
    }
    
    /** */
    private String getConnectStr(Connection data) {
        String connect;
        if (data.isServerMode()) {
            connect = "Srvr=\"" + data.getServer() + "\"; Ref=\"" + data.getBase() + "\"";
        } else {
            connect = "File=\"" + data.getFilepath() + "\"";
        }//end if-else

        return connect;
    }

    /**
     * @param server
     * @param port
     * @param connect
     * @param user
     * @param pass
     * @return
     * @throws Exception
     */
    private String getCompany(String server, String port, String connect, 
                              String user, String pass) throws Exception {
        String res = null;
        Client client;
        try {
            String m_server = "localhost";
            if (server != null && !server.isEmpty()) {
                m_server = server;
            }//end if

            int m_port = 80;
            if (port != null && !port.isEmpty()) {
                m_port = Integer.valueOf(port);
            }//end if

            client = new Client(m_server, m_port, "J1C");
            Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, "Connect to server");
            if (client.connect()) {
                StringBuilder sb = new StringBuilder();
                sb.append(ChatMessage.GETFIRMIND).append(ChatMessage.delim);
                sb.append(connect);
                sb.append(ChatMessage.delim).append(user == null ? "" : user);
                sb.append(ChatMessage.delim).append(pass == null ? "" : pass);
                sb.append(ChatMessage.delim);

                Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, "Exchange from server");
                client.messageExchange(sb.toString());

                ChatMessage msg;
                while ((msg = client.getMessage()) == null) {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {}
                }
                client.disconnect();
                client.stop();

                // the messaage part of the ChatMessage
                String message = msg.getMessage();
                Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, "Message {0}", message);

                // Switch on the type of message receive
                switch (msg.getType()) {
                    case ChatMessage.RESULT:
                        res = message;
                        break;
                    case ChatMessage.LOGOUT:
                        res = null;
                        break;
                    case ChatMessage.ERROR:
                        res = message;
                        throw new Exception(res);
                }//end switch
            }
        } catch (Throwable e) {
            throw new Exception(e);
        }//end try
        return res;
    }
    
    private void showSplash(final Stage initStage, Task<?> task, InitCompletionHandler initCompletionHandler) {        
        task.messageProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(
                    ObservableValue<? extends String> observable,
                    String oldValue, String newValue) {
                progressTextArea.appendText(observable.getValue());
            }
        });
        loadProgress.progressProperty().bind(task.progressProperty());
        progressStop.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                task.cancel();
            }
        });
        task.stateProperty().addListener(new ChangeListener<Worker.State>() {
            @Override
            public void changed(ObservableValue<? extends Worker.State> observableValue,
                                Worker.State oldState,
                                Worker.State newState) {
                
                if (newState == Worker.State.SUCCEEDED ||
                    newState == Worker.State.FAILED ||
                    newState == Worker.State.CANCELLED) {
                    
                    Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, newState.name());
                    
                    loadProgress.progressProperty().unbind();
                    loadProgress.setProgress(1);
                    initStage.toFront();
                    FadeTransition fadeSplash = new FadeTransition(Duration.seconds(1.2), splashLayout);
                    fadeSplash.setFromValue(1.0);
                    fadeSplash.setToValue(0.0);
                    fadeSplash.setOnFinished(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent actionEvent) {
                            Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, "Hide stage");
                            initStage.hide();
                        }
                    });
                    fadeSplash.play();
                    
                    switch (newState) {
                        case FAILED:
                            Throwable exception = task.getException();
                            Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, null, exception);
                            initCompletionHandler.failed(exception.getMessage());
                            break;
                        case SUCCEEDED:
                            Object value = task.getValue();
                            Logger.getLogger(Mana.class.getName()).log(Level.INFO, value.toString());
                            initCompletionHandler.complete(value);
                            break;
                        case CANCELLED:
                            Logger.getLogger(Mana.class.getName()).log(Level.INFO, "Task canceled");
                            initCompletionHandler.cancelled();
                            break;
                    }
                }
            }
        });

        Scene splashScene = new Scene(splashLayout);
        initStage.initStyle(StageStyle.UNDECORATED);
        initStage.initModality(Modality.APPLICATION_MODAL);
        final Rectangle2D bounds = Screen.getPrimary().getBounds();
        initStage.setScene(splashScene);
        initStage.setX(bounds.getMinX() + bounds.getWidth() / 2 - SPLASH_WIDTH / 2);
        initStage.setY(bounds.getMinY() + bounds.getHeight() / 2 - SPLASH_HEIGHT / 2);
        initStage.show();
    }
    
    public interface InitCompletionHandler {
        public void complete(Object value);
        public void failed(String error);
        public void cancelled();
    }
}
