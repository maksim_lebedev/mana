package cl.itf.mana.client.util;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javafx.beans.InvalidationListener;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.ListBinding;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;

/**
 * @author max
 */
public class ManaValidation {
    
    public BooleanBinding emptyTextFieldBinding(TextField textField, String message, Map<BooleanBinding, String> messages) {
        BooleanBinding binding = Bindings.createBooleanBinding(() -> {
            final String text = textField.getText();
            return text == null || textField.getText().trim().isEmpty();
        }, textField.textProperty());
        configureTextFieldBinding(binding, textField, message, messages);
        return binding;
    }

    public BooleanBinding patternTextFieldBinding(TextField textField, Pattern pattern, String message, Map<BooleanBinding, String> messages) {
        BooleanBinding binding = Bindings.createBooleanBinding(() -> !pattern.matcher(textField.getText()).matches(), textField.textProperty());
        configureTextFieldBinding(binding, textField, message, messages);
        return binding;
    }

    public void bindMessageLabels(BooleanBinding[] validationBindings, List<Node> labelList, Map<BooleanBinding, String> messages) {
        ListBinding<Node> nodeListBinding = new ListBinding<Node>() {
            {
                InvalidationListener invalidationListener = obs -> invalidate();
                Arrays.stream(validationBindings).forEach(binding -> binding.addListener(invalidationListener));
            }

            @Override
            protected ObservableList<Node> computeValue() {
                return FXCollections.observableArrayList();
//                return FXCollections.observableList(Arrays.stream(validationBindings).filter(BooleanBinding::get)
//                                                    .map(messages::get).map(Label::new).collect(Collectors.toList()));
            }
        };

        Bindings.bindContent(labelList, nodeListBinding);
    }
    
    private void configureTextFieldBinding(BooleanBinding binding, TextField textField, String message, Map<BooleanBinding, String> messages) {
        messages.put(binding, message);
        if (textField.getTooltip() == null) {
            textField.setTooltip(new Tooltip());
        }
        String tooltipText = textField.getTooltip().getText();
        binding.addListener((obs, oldValue, newValue) -> {
            updateTextFieldValidationStatus(textField, tooltipText, newValue, message);
        });
        updateTextFieldValidationStatus(textField, tooltipText, binding.get(), message);
    }

    public void updateTextFieldValidationStatus(Control textField, String defaultTooltipText, boolean invalid, String message) {
        textField.pseudoClassStateChanged(PseudoClass.getPseudoClass("validation-error"), invalid);
        String tooltipText;
        if (invalid) {
            tooltipText = message;
        } else {
            tooltipText = defaultTooltipText;
        }
        if (tooltipText == null || tooltipText.isEmpty()) {
            textField.setTooltip(null);
        } else {
            Tooltip tooltip = textField.getTooltip();
            if (tooltip == null) {
                textField.setTooltip(new Tooltip(tooltipText));
            } else {
                tooltip.setText(tooltipText);
            }
        }
    }
}
