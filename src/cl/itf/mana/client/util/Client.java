package cl.itf.mana.client.util;

import cl.itf.j1c.chatmessage.ChatMessage;
import java.net.*;
import java.io.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * The Client that can be run both as a console or a GUI
 */
public class Client {

    protected Logger logger = LoggerFactory.getLogger(Client.class);

    // for I/O
    private ObjectInputStream sInput;	// to read from the socket
    private ObjectOutputStream sOutput;	// to write on the socket
    private Socket socket;
    private ListenFromServer TH;
    private ChatMessage comp;
    // the server, the port and the username
    private final String server, username;
    private final int port;

    /*
     *  Constructor called by console mode
     *  server: the server address
     *  port: the port number
     *  username: the username
     */
    public Client(String server, int port, String username) {
        this.server = server;
        this.port = port;
        this.username = username;
    }

    /**
     * @return
     */
    public boolean stop() {
        TH.close();
        return true;
    }

    public boolean connect() throws IOException {
        // try to connect to the server
        try {
            socket = new Socket(server, port);
        } // if it failed not much I can so
        catch (IOException ec) {
            display("Error connectiong to server:" + ec);
            throw ec;
        }

        String msg = "Connection accepted " + socket.getInetAddress() + ":" + socket.getPort();
        display(msg);
        return true;
    }

    public void messageExchange(String message) {
        // creates the Thread to listen from the server 
        TH = new ListenFromServer(message);
        Thread t = new Thread(TH);
        t.start();
    }

    /*
     * To send a message to the console or the GUI
     */
    private void display(String msg) {
        logger.debug(msg);
    }

    /*
     * To send a message to the server
     */
    public synchronized ChatMessage getMessage() {
        return comp;
    }

    /*
     * When something goes wrong
     * Close the Input/Output streams and disconnect not much to do in the catch clause
     */
    public void disconnect() {
        try {
            sInput.close();
        } catch (IOException e) {
            logger.error(e.getMessage());
        }//end try

        try {
            sOutput.close();
        } catch (IOException e) {
            logger.error(e.getMessage());
        }//end try

        try {
            socket.close();
        } catch (IOException e) {
            logger.error(e.getMessage());
        }//end try
    }

    /*
     * A class that waits for the message from the server and append them to the JTextArea
     * if we have a GUI or simply System.out.println() it in console mode
     */
    class ListenFromServer implements Runnable {

        private final String message;

        public ListenFromServer(String message) {
            this.message = message;
        }

        @Override
        public void run() {
            /* Creating both Data Stream */
            try {
                sInput = new ObjectInputStream(socket.getInputStream());
                sOutput = new ObjectOutputStream(socket.getOutputStream());
            } catch (IOException eIO) {
                display("Exception creating new Input/output Streams: " + eIO);
            }
            // Send our username to the server this is the only message that we
            // will send as a String. All other messages will be ChatMessage objects
            try {
                sOutput.writeObject(username);
                ChatMessage mes = (ChatMessage) sInput.readObject();
                if (mes.getType() == ChatMessage.ERROR) {
                    throw new IOException("Не удалось соединиться с сервером");
                }
                if (mes.getType() == ChatMessage.ACCEPTED) {
                    display("Соединение установлено");
                }
            } catch (IOException eIO) {
                display("Exception doing login : " + eIO);
                disconnect();
            } catch (ClassNotFoundException ex) {
                display("Exception doing login : " + ex);
            }

            obmen();
        }

        public void obmen() {
            try {
                display("> J1C wait server");
                sOutput.writeObject(new ChatMessage(ChatMessage.MESSAGE, message));
                comp = (ChatMessage) sInput.readObject();
            } catch (IOException e) {
                comp = new ChatMessage(ChatMessage.ERROR, " Exception reading Streams: " + e);
                display(username + " Exception reading Streams: " + e);
            } catch (ClassNotFoundException e2) {
                comp = new ChatMessage(ChatMessage.ERROR, " Exception reading Streams: " + e2);
                display(e2.getMessage());
            }//end try
        }
        
        public void close() {
            // try to close the connection
            try {
                if (sOutput != null) {
                    sOutput.close();
                }
            } catch (IOException e) {
                logger.error(e.getMessage());
            }//end try

            try {
                if (sInput != null) {
                    sInput.close();
                }
            } catch (IOException e) {
                logger.error(e.getMessage());
            }//end try

            try {
                if (socket != null) {
                    socket.close();
                }
            } catch (IOException e) {
                logger.error(e.getMessage());
            }//end try
        }
    }
}
