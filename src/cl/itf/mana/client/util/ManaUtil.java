package cl.itf.mana.client.util;

import cl.itf.mana.client.model.Base;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author lebedev-m
 */
public class ManaUtil {

    public static String getKvartal() {
        final Calendar c = Calendar.getInstance();

        int mMonth = c.get(Calendar.MONTH) + 1;
        int mYear = c.get(Calendar.YEAR);
        int mDayOfMonth = c.getActualMaximum(Calendar.DAY_OF_MONTH);

        StringBuilder today = new StringBuilder();

//        Calendar calendar = new GregorianCalendar();
// 
//        /* Определение текущей даты, принимаемой за "сегодня". Относительно неё пойдет остальной расчет. */
//        SimpleDateFormat formattedDate = new SimpleDateFormat("dd-MM-yyyy");
//        String dateToday = formattedDate.format(calendar.getTime()); //Дата текущая
// 
//        /* Определение предыдущей даты */
//        calendar.add(Calendar.DAY_OF_MONTH, -1);
//        String dateYesterday = formattedDate.format(calendar.getTime()); //Дата предыдущая
//        calendar.add(Calendar.DAY_OF_MONTH, 1);
// 
//        /* Определение даты на начало текущей недели */
//        Integer dayDifference = calendar.get(Calendar.DAY_OF_WEEK) == 1 ? -6 : (Calendar.MONDAY - calendar.get(Calendar.DAY_OF_WEEK));
//        calendar.add(Calendar.DAY_OF_MONTH, dayDifference);
//        String dateFirstDayOfWeek = formattedDate.format(calendar.getTime()); //Дата на начало текущей недели
//        calendar.add(Calendar.DAY_OF_MONTH, -dayDifference);
// 
//        /* Определение даты на начало текущего месяца */
//        calendar.set(Calendar.DAY_OF_MONTH, 1);
//        String dateFirstDayOfMonth = formattedDate.format(calendar.getTime()); //Дата на начало текущего месяца*/
// 
//        /* Определение даты на начало текущего квартала */
//        Integer monthDifference = 0;
//        if (calendar.get(Calendar.MONTH) == Calendar.JANUARY || calendar.get(Calendar.MONTH) == Calendar.APRIL ||
//                calendar.get(Calendar.MONTH) == Calendar.JULY || calendar.get(Calendar.MONTH) == Calendar.OCTOBER) {
//            monthDifference = 0;
//        } else if (calendar.get(Calendar.MONTH) == Calendar.FEBRUARY || calendar.get(Calendar.MONTH) == Calendar.MAY ||
//                calendar.get(Calendar.MONTH) == Calendar.AUGUST || calendar.get(Calendar.MONTH) == Calendar.NOVEMBER) {
//            monthDifference = -1;
//        } else if (calendar.get(Calendar.MONTH) == Calendar.MARCH || calendar.get(Calendar.MONTH) == Calendar.JUNE ||
//                calendar.get(Calendar.MONTH) == Calendar.SEPTEMBER || calendar.get(Calendar.MONTH) == Calendar.DECEMBER) {
//            monthDifference = -2;
//        } else {
//            logger.Info("getDates(): Не удалось определить месяц.");
//        }
//        calendar.add(Calendar.MONTH, monthDifference);
//        String dateFirstDayOfQuarter = formattedDate.format(calendar.getTime()); //Дата на начало текущего квартала
//        calendar.add(Calendar.MONTH, -monthDifference);
        
        if (mMonth == 1 || mMonth == 2 || mMonth == 3) {
            today = today.append("01-01-").append(mYear).append("/")
                         .append(mDayOfMonth).append("-03-").append(mYear);
        }

        if (mMonth == 4 || mMonth == 5 || mMonth == 6) {
            today = today.append("01-04-").append(mYear).append("/")
                         .append(mDayOfMonth).append("-06-").append(mYear);
        }

        if (mMonth == 7 || mMonth == 8 || mMonth == 9) {
            today = today.append("01-07-").append(mYear).append("/")
                         .append(mDayOfMonth).append("-09-").append(mYear);
        }

        if (mMonth == 10 || mMonth == 11 || mMonth == 12) {
            today = today.append("01-10-").append(mYear).append("/")
                         .append(mDayOfMonth).append("-12-").append(mYear);
        }

        return today.toString();
    }
    
    public static String getPrettyDate(long timemillis) {
        final DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        return sdf.format(new Date(timemillis));
    }
    
    public void exportDB(File filename, List<Base> rows) {
        try {
            if (!filename.exists()) {
                filename.createNewFile();
            }//end if
            final GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setPrettyPrinting();
            final Gson gson = gsonBuilder.create();
            
            final String json = gson.toJson(rows);
            try (FileWriter writer = new FileWriter(filename)) {
                writer.write(json);
            }
        } catch (Throwable ex) {
            Logger.getLogger(ManaUtil.class.getName()).log(Level.SEVERE, null, ex);
        }//end try
    }
    
    public List<Base> importDB(File path) {
        ArrayList<Base> listBase;
        if (path.exists()) {
            final Gson gson = new Gson();
            try (BufferedReader br = new BufferedReader(new FileReader(path))) {
                final TypeToken token = new TypeToken<ArrayList<Base>>(){};
                listBase = (ArrayList<Base>) gson.fromJson(br, token.getType());
            } catch (IOException ex) {
                Logger.getLogger(ManaUtil.class.getName()).log(Level.SEVERE, null, ex);
                listBase = new ArrayList<>();
            }
        } else {
            listBase = new ArrayList<>();
        }//end if-else
        
        return listBase;
    }
}
